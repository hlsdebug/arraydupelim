/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/* crypto/bf/bf_cfb64.c */
/* Copyright (C) 1995-1997 Eric Young (eay@mincom.oz.au)
 * All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@mincom.oz.au).
 * The implementation was written so as to conform with Netscapes SSL.
 * 
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@mincom.oz.au).
 * 
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes cryptographic software written by
 *     Eric Young (eay@mincom.oz.au)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from 
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@mincom.oz.au)"
 * 
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 * 
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */
/* The input and output encrypted as though 64bit cfb mode is being
 * used.  The extra state information to record how much of the
 * 64bit block we have used is contained in *num;
 */
#include "blowfish.h"

void BF_cfb64_encrypt(in,out,length,ivec,num,encrypt)
unsigned char *in;
unsigned char *out;
long length;
unsigned char *ivec;
int *num;
int encrypt;
{
  register unsigned long v0;
  register unsigned long v1;
  register unsigned long t;
  register int n;
  register long l;
  unsigned long ti[2];
  unsigned char *iv;
  unsigned char c;
  unsigned char cc;
  n =  *num;
  pushDbg(n,902);
  l = length;
  pushDbgLong(l,903);
  iv = ((unsigned char *)ivec);
  if (encrypt) {
    while(l--){
      if (n == 0) {
        n2l(iv,v0);
        unsigned long *temp_ptr_ti_95_108;
        temp_ptr_ti_95_108 = &ti[0];
        pushDbgLong( *temp_ptr_ti_95_108,906);
         *temp_ptr_ti_95_108 = v0;
        n2l(iv,v1);
        unsigned long *temp_ptr_ti_95_110;
        temp_ptr_ti_95_110 = &ti[1];
        pushDbgLong( *temp_ptr_ti_95_110,908);
         *temp_ptr_ti_95_110 = v1;
        BF_encrypt(((unsigned long *)ti),1);
        iv = ((unsigned char *)ivec);
        t = ti[0];
        pushDbgLong(t,911);
        l2n(t,iv);
        t = ti[1];
        pushDbgLong(t,913);
        l2n(t,iv);
        iv = ((unsigned char *)ivec);
      }
      c = (( *(in++)) ^ iv[n]);
      pushDbg(c,916);
      unsigned char *temp_ptr_out_0_121;
      temp_ptr_out_0_121 = out++;
      pushDbg( *temp_ptr_out_0_121,919);
       *temp_ptr_out_0_121 = c;
      unsigned char *temp_ptr_iv_96_122;
      temp_ptr_iv_96_122 = &iv[n];
      pushDbg( *temp_ptr_iv_96_122,920);
       *temp_ptr_iv_96_122 = c;
      n = n + 1 & 0x07;
      pushDbg(n,922);
    }
    pushDbgLong(l,12920);
  }
   else {
    while(l--){
      if (n == 0) {
        n2l(iv,v0);
        unsigned long *temp_ptr_ti_95_133;
        temp_ptr_ti_95_133 = &ti[0];
        pushDbgLong( *temp_ptr_ti_95_133,926);
         *temp_ptr_ti_95_133 = v0;
        n2l(iv,v1);
        unsigned long *temp_ptr_ti_95_135;
        temp_ptr_ti_95_135 = &ti[1];
        pushDbgLong( *temp_ptr_ti_95_135,928);
         *temp_ptr_ti_95_135 = v1;
        BF_encrypt(((unsigned long *)ti),1);
        iv = ((unsigned char *)ivec);
        t = ti[0];
        pushDbgLong(t,931);
        l2n(t,iv);
        t = ti[1];
        pushDbgLong(t,933);
        l2n(t,iv);
        iv = ((unsigned char *)ivec);
      }
      cc =  *(in++);
      pushDbg(cc,936);
      c = iv[n];
      pushDbg(c,937);
      unsigned char *temp_ptr_iv_96_146;
      temp_ptr_iv_96_146 = &iv[n];
      pushDbg( *temp_ptr_iv_96_146,939);
       *temp_ptr_iv_96_146 = cc;
      unsigned char *temp_ptr_out_0_147;
      temp_ptr_out_0_147 = out++;
      pushDbg( *temp_ptr_out_0_147,941);
       *temp_ptr_out_0_147 = (c ^ cc);
      n = n + 1 & 0x07;
      pushDbg(n,943);
    }
    pushDbgLong(l,12931);
  }
  v0 = v1 = ti[0] = ti[1] = t = (c = cc = 0);
  pushDbg(cc,954);
  pushDbgLong(t,952);
  int *temp_ptr_num_0_152;
  temp_ptr_num_0_152 = num;
  pushDbg( *temp_ptr_num_0_152,955);
   *temp_ptr_num_0_152 = n;
}
