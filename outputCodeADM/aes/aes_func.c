/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/* aes_function.c */
/*
 * Copyright (C) 2005
 * Akira Iwata & Masayuki Sato
 * Akira Iwata Laboratory,
 * Nagoya Institute of Technology in Japan.
 *
 * All rights reserved.
 *
 * This software is written by Masayuki Sato.
 * And if you want to contact us, send an email to Kimitake Wakayama
 * (wakayama@elcom.nitech.ac.jp)
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. All advertising materials mentioning features or use of this software must
 *    display the following acknowledgment:
 *    "This product includes software developed by Akira Iwata Laboratory,
 *    Nagoya Institute of Technology in Japan (http://mars.elcom.nitech.ac.jp/)."
 *
 * 4. Redistributions of any form whatsoever must retain the following
 *    acknowledgment:
 *    "This product includes software developed by Akira Iwata Laboratory,
 *     Nagoya Institute of Technology in Japan (http://mars.elcom.nitech.ac.jp/)."
 *
 *   THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY.
 *   AKIRA IWATA LABORATORY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 *   SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS,
 *   IN NO EVENT SHALL AKIRA IWATA LABORATORY BE LIABLE FOR ANY SPECIAL,
 *   INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
 *   FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 *   NEGLIGENCE OR OTHER TORTUOUS ACTION, ARISING OUT OF OR IN CONNECTION
 *   WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "aes.h"
const int Sbox[16][16] = {{(0x63), (0x7c), (0x77), (0x7b), (0xf2), (0x6b), (0x6f), (0xc5), (0x30), (0x01), (0x67), (0x2b), (0xfe), (0xd7), (0xab), (0x76)}, {(0xca), (0x82), (0xc9), (0x7d), (0xfa), (0x59), (0x47), (0xf0), (0xad), (0xd4), (0xa2), (0xaf), (0x9c), (0xa4), (0x72), (0xc0)}, {(0xb7), (0xfd), (0x93), (0x26), (0x36), (0x3f), (0xf7), (0xcc), (0x34), (0xa5), (0xe5), (0xf1), (0x71), (0xd8), (0x31), (0x15)}, {(0x04), (0xc7), (0x23), (0xc3), (0x18), (0x96), (0x05), (0x9a), (0x07), (0x12), (0x80), (0xe2), (0xeb), (0x27), (0xb2), (0x75)}, {(0x09), (0x83), (0x2c), (0x1a), (0x1b), (0x6e), (0x5a), (0xa0), (0x52), (0x3b), (0xd6), (0xb3), (0x29), (0xe3), (0x2f), (0x84)}, {(0x53), (0xd1), (0x00), (0xed), (0x20), (0xfc), (0xb1), (0x5b), (0x6a), (0xcb), (0xbe), (0x39), (0x4a), (0x4c), (0x58), (0xcf)}, {(0xd0), (0xef), (0xaa), (0xfb), (0x43), (0x4d), (0x33), (0x85), (0x45), (0xf9), (0x02), (0x7f), (0x50), (0x3c), (0x9f), (0xa8)}, {(0x51), (0xa3), (0x40), (0x8f), (0x92), (0x9d), (0x38), (0xf5), (0xbc), (0xb6), (0xda), (0x21), (0x10), (0xff), (0xf3), (0xd2)}, {(0xcd), (0x0c), (0x13), (0xec), (0x5f), (0x97), (0x44), (0x17), (0xc4), (0xa7), (0x7e), (0x3d), (0x64), (0x5d), (0x19), (0x73)}, {(0x60), (0x81), (0x4f), (0xdc), (0x22), (0x2a), (0x90), (0x88), (0x46), (0xee), (0xb8), (0x14), (0xde), (0x5e), (0x0b), (0xdb)}, {(0xe0), (0x32), (0x3a), (0x0a), (0x49), (0x06), (0x24), (0x5c), (0xc2), (0xd3), (0xac), (0x62), (0x91), (0x95), (0xe4), (0x79)}, {(0xe7), (0xc8), (0x37), (0x6d), (0x8d), (0xd5), (0x4e), (0xa9), (0x6c), (0x56), (0xf4), (0xea), (0x65), (0x7a), (0xae), (0x08)}, {(0xba), (0x78), (0x25), (0x2e), (0x1c), (0xa6), (0xb4), (0xc6), (0xe8), (0xdd), (0x74), (0x1f), (0x4b), (0xbd), (0x8b), (0x8a)}, {(0x70), (0x3e), (0xb5), (0x66), (0x48), (0x03), (0xf6), (0x0e), (0x61), (0x35), (0x57), (0xb9), (0x86), (0xc1), (0x1d), (0x9e)}, {(0xe1), (0xf8), (0x98), (0x11), (0x69), (0xd9), (0x8e), (0x94), (0x9b), (0x1e), (0x87), (0xe9), (0xce), (0x55), (0x28), (0xdf)}, {(0x8c), (0xa1), (0x89), (0x0d), (0xbf), (0xe6), (0x42), (0x68), (0x41), (0x99), (0x2d), (0x0f), (0xb0), (0x54), (0xbb), (0x16)}};
const int invSbox[16][16] = {{(0x52), (0x09), (0x6a), (0xd5), (0x30), (0x36), (0xa5), (0x38), (0xbf), (0x40), (0xa3), (0x9e), (0x81), (0xf3), (0xd7), (0xfb)}, {(0x7c), (0xe3), (0x39), (0x82), (0x9b), (0x2f), (0xff), (0x87), (0x34), (0x8e), (0x43), (0x44), (0xc4), (0xde), (0xe9), (0xcb)}, {(0x54), (0x7b), (0x94), (0x32), (0xa6), (0xc2), (0x23), (0x3d), (0xee), (0x4c), (0x95), (0x0b), (0x42), (0xfa), (0xc3), (0x4e)}, {(0x08), (0x2e), (0xa1), (0x66), (0x28), (0xd9), (0x24), (0xb2), (0x76), (0x5b), (0xa2), (0x49), (0x6d), (0x8b), (0xd1), (0x25)}, {(0x72), (0xf8), (0xf6), (0x64), (0x86), (0x68), (0x98), (0x16), (0xd4), (0xa4), (0x5c), (0xcc), (0x5d), (0x65), (0xb6), (0x92)}, {(0x6c), (0x70), (0x48), (0x50), (0xfd), (0xed), (0xb9), (0xda), (0x5e), (0x15), (0x46), (0x57), (0xa7), (0x8d), (0x9d), (0x84)}, {(0x90), (0xd8), (0xab), (0x00), (0x8c), (0xbc), (0xd3), (0x0a), (0xf7), (0xe4), (0x58), (0x05), (0xb8), (0xb3), (0x45), (0x06)}, {(0xd0), (0x2c), (0x1e), (0x8f), (0xca), (0x3f), (0x0f), (0x02), (0xc1), (0xaf), (0xbd), (0x03), (0x01), (0x13), (0x8a), (0x6b)}, {(0x3a), (0x91), (0x11), (0x41), (0x4f), (0x67), (0xdc), (0xea), (0x97), (0xf2), (0xcf), (0xce), (0xf0), (0xb4), (0xe6), (0x73)}, {(0x96), (0xac), (0x74), (0x22), (0xe7), (0xad), (0x35), (0x85), (0xe2), (0xf9), (0x37), (0xe8), (0x1c), (0x75), (0xdf), (0x6e)}, {(0x47), (0xf1), (0x1a), (0x71), (0x1d), (0x29), (0xc5), (0x89), (0x6f), (0xb7), (0x62), (0x0e), (0xaa), (0x18), (0xbe), (0x1b)}, {(0xfc), (0x56), (0x3e), (0x4b), (0xc6), (0xd2), (0x79), (0x20), (0x9a), (0xdb), (0xc0), (0xfe), (0x78), (0xcd), (0x5a), (0xf4)}, {(0x1f), (0xdd), (0xa8), (0x33), (0x88), (0x07), (0xc7), (0x31), (0xb1), (0x12), (0x10), (0x59), (0x27), (0x80), (0xec), (0x5f)}, {(0x60), (0x51), (0x7f), (0xa9), (0x19), (0xb5), (0x4a), (0x0d), (0x2d), (0xe5), (0x7a), (0x9f), (0x93), (0xc9), (0x9c), (0xef)}, {(0xa0), (0xe0), (0x3b), (0x4d), (0xae), (0x2a), (0xf5), (0xb0), (0xc8), (0xeb), (0xbb), (0x3c), (0x83), (0x53), (0x99), (0x61)}, {(0x17), (0x2b), (0x04), (0x7e), (0xba), (0x77), (0xd6), (0x26), (0xe1), (0x69), (0x14), (0x63), (0x55), (0x21), (0x0c), (0x7d)}};
/* ********* ByteSub & ShiftRow ********* */

void ByteSub_ShiftRow(int statemt[32],int nb)
{
  int temp;
  switch(nb){
    case 4:
{
      temp = Sbox[statemt[1] >> 4][statemt[1] & 0xf];
      pushDbg(temp,1927);
      int *temp_ptr_statemt_136_144;
      temp_ptr_statemt_136_144 = &statemt[1];
      pushDbg( *temp_ptr_statemt_136_144,1934);
       *temp_ptr_statemt_136_144 = Sbox[statemt[5] >> 4][statemt[5] & 0xf];
      int *temp_ptr_statemt_136_145;
      temp_ptr_statemt_136_145 = &statemt[5];
      pushDbg( *temp_ptr_statemt_136_145,1942);
       *temp_ptr_statemt_136_145 = Sbox[statemt[9] >> 4][statemt[9] & 0xf];
      int *temp_ptr_statemt_136_146;
      temp_ptr_statemt_136_146 = &statemt[9];
      pushDbg( *temp_ptr_statemt_136_146,1950);
       *temp_ptr_statemt_136_146 = Sbox[statemt[13] >> 4][statemt[13] & 0xf];
      int *temp_ptr_statemt_136_147;
      temp_ptr_statemt_136_147 = &statemt[13];
      pushDbg( *temp_ptr_statemt_136_147,1958);
       *temp_ptr_statemt_136_147 = temp;
      temp = Sbox[statemt[2] >> 4][statemt[2] & 0xf];
      pushDbg(temp,1960);
      int *temp_ptr_statemt_136_150;
      temp_ptr_statemt_136_150 = &statemt[2];
      pushDbg( *temp_ptr_statemt_136_150,1967);
       *temp_ptr_statemt_136_150 = Sbox[statemt[10] >> 4][statemt[10] & 0xf];
      int *temp_ptr_statemt_136_151;
      temp_ptr_statemt_136_151 = &statemt[10];
      pushDbg( *temp_ptr_statemt_136_151,1975);
       *temp_ptr_statemt_136_151 = temp;
      temp = Sbox[statemt[6] >> 4][statemt[6] & 0xf];
      pushDbg(temp,1977);
      int *temp_ptr_statemt_136_153;
      temp_ptr_statemt_136_153 = &statemt[6];
      pushDbg( *temp_ptr_statemt_136_153,1984);
       *temp_ptr_statemt_136_153 = Sbox[statemt[14] >> 4][statemt[14] & 0xf];
      int *temp_ptr_statemt_136_154;
      temp_ptr_statemt_136_154 = &statemt[14];
      pushDbg( *temp_ptr_statemt_136_154,1992);
       *temp_ptr_statemt_136_154 = temp;
      temp = Sbox[statemt[3] >> 4][statemt[3] & 0xf];
      pushDbg(temp,1994);
      int *temp_ptr_statemt_136_157;
      temp_ptr_statemt_136_157 = &statemt[3];
      pushDbg( *temp_ptr_statemt_136_157,2001);
       *temp_ptr_statemt_136_157 = Sbox[statemt[15] >> 4][statemt[15] & 0xf];
      int *temp_ptr_statemt_136_158;
      temp_ptr_statemt_136_158 = &statemt[15];
      pushDbg( *temp_ptr_statemt_136_158,2009);
       *temp_ptr_statemt_136_158 = Sbox[statemt[11] >> 4][statemt[11] & 0xf];
      int *temp_ptr_statemt_136_159;
      temp_ptr_statemt_136_159 = &statemt[11];
      pushDbg( *temp_ptr_statemt_136_159,2017);
       *temp_ptr_statemt_136_159 = Sbox[statemt[7] >> 4][statemt[7] & 0xf];
      int *temp_ptr_statemt_136_160;
      temp_ptr_statemt_136_160 = &statemt[7];
      pushDbg( *temp_ptr_statemt_136_160,2025);
       *temp_ptr_statemt_136_160 = temp;
      int *temp_ptr_statemt_136_162;
      temp_ptr_statemt_136_162 = &statemt[0];
      pushDbg( *temp_ptr_statemt_136_162,2027);
       *temp_ptr_statemt_136_162 = Sbox[statemt[0] >> 4][statemt[0] & 0xf];
      int *temp_ptr_statemt_136_163;
      temp_ptr_statemt_136_163 = &statemt[4];
      pushDbg( *temp_ptr_statemt_136_163,2035);
       *temp_ptr_statemt_136_163 = Sbox[statemt[4] >> 4][statemt[4] & 0xf];
      int *temp_ptr_statemt_136_164;
      temp_ptr_statemt_136_164 = &statemt[8];
      pushDbg( *temp_ptr_statemt_136_164,2043);
       *temp_ptr_statemt_136_164 = Sbox[statemt[8] >> 4][statemt[8] & 0xf];
      int *temp_ptr_statemt_136_165;
      temp_ptr_statemt_136_165 = &statemt[12];
      pushDbg( *temp_ptr_statemt_136_165,2051);
       *temp_ptr_statemt_136_165 = Sbox[statemt[12] >> 4][statemt[12] & 0xf];
      break; 
    }
    case 6:
{
      temp = Sbox[statemt[1] >> 4][statemt[1] & 0xf];
      pushDbg(temp,2059);
      int *temp_ptr_statemt_136_169;
      temp_ptr_statemt_136_169 = &statemt[1];
      pushDbg( *temp_ptr_statemt_136_169,2066);
       *temp_ptr_statemt_136_169 = Sbox[statemt[5] >> 4][statemt[5] & 0xf];
      int *temp_ptr_statemt_136_170;
      temp_ptr_statemt_136_170 = &statemt[5];
      pushDbg( *temp_ptr_statemt_136_170,2074);
       *temp_ptr_statemt_136_170 = Sbox[statemt[9] >> 4][statemt[9] & 0xf];
      int *temp_ptr_statemt_136_171;
      temp_ptr_statemt_136_171 = &statemt[9];
      pushDbg( *temp_ptr_statemt_136_171,2082);
       *temp_ptr_statemt_136_171 = Sbox[statemt[13] >> 4][statemt[13] & 0xf];
      int *temp_ptr_statemt_136_172;
      temp_ptr_statemt_136_172 = &statemt[13];
      pushDbg( *temp_ptr_statemt_136_172,2090);
       *temp_ptr_statemt_136_172 = Sbox[statemt[17] >> 4][statemt[17] & 0xf];
      int *temp_ptr_statemt_136_173;
      temp_ptr_statemt_136_173 = &statemt[17];
      pushDbg( *temp_ptr_statemt_136_173,2098);
       *temp_ptr_statemt_136_173 = Sbox[statemt[21] >> 4][statemt[21] & 0xf];
      int *temp_ptr_statemt_136_174;
      temp_ptr_statemt_136_174 = &statemt[21];
      pushDbg( *temp_ptr_statemt_136_174,2106);
       *temp_ptr_statemt_136_174 = temp;
      temp = Sbox[statemt[2] >> 4][statemt[2] & 0xf];
      pushDbg(temp,2108);
      int *temp_ptr_statemt_136_177;
      temp_ptr_statemt_136_177 = &statemt[2];
      pushDbg( *temp_ptr_statemt_136_177,2115);
       *temp_ptr_statemt_136_177 = Sbox[statemt[10] >> 4][statemt[10] & 0xf];
      int *temp_ptr_statemt_136_178;
      temp_ptr_statemt_136_178 = &statemt[10];
      pushDbg( *temp_ptr_statemt_136_178,2123);
       *temp_ptr_statemt_136_178 = Sbox[statemt[18] >> 4][statemt[18] & 0xf];
      int *temp_ptr_statemt_136_179;
      temp_ptr_statemt_136_179 = &statemt[18];
      pushDbg( *temp_ptr_statemt_136_179,2131);
       *temp_ptr_statemt_136_179 = temp;
      temp = Sbox[statemt[6] >> 4][statemt[6] & 0xf];
      pushDbg(temp,2133);
      int *temp_ptr_statemt_136_181;
      temp_ptr_statemt_136_181 = &statemt[6];
      pushDbg( *temp_ptr_statemt_136_181,2140);
       *temp_ptr_statemt_136_181 = Sbox[statemt[14] >> 4][statemt[14] & 0xf];
      int *temp_ptr_statemt_136_182;
      temp_ptr_statemt_136_182 = &statemt[14];
      pushDbg( *temp_ptr_statemt_136_182,2148);
       *temp_ptr_statemt_136_182 = Sbox[statemt[22] >> 4][statemt[22] & 0xf];
      int *temp_ptr_statemt_136_183;
      temp_ptr_statemt_136_183 = &statemt[22];
      pushDbg( *temp_ptr_statemt_136_183,2156);
       *temp_ptr_statemt_136_183 = temp;
      temp = Sbox[statemt[3] >> 4][statemt[3] & 0xf];
      pushDbg(temp,2158);
      int *temp_ptr_statemt_136_186;
      temp_ptr_statemt_136_186 = &statemt[3];
      pushDbg( *temp_ptr_statemt_136_186,2165);
       *temp_ptr_statemt_136_186 = Sbox[statemt[15] >> 4][statemt[15] & 0xf];
      int *temp_ptr_statemt_136_187;
      temp_ptr_statemt_136_187 = &statemt[15];
      pushDbg( *temp_ptr_statemt_136_187,2173);
       *temp_ptr_statemt_136_187 = temp;
      temp = Sbox[statemt[7] >> 4][statemt[7] & 0xf];
      pushDbg(temp,2175);
      int *temp_ptr_statemt_136_189;
      temp_ptr_statemt_136_189 = &statemt[7];
      pushDbg( *temp_ptr_statemt_136_189,2182);
       *temp_ptr_statemt_136_189 = Sbox[statemt[19] >> 4][statemt[19] & 0xf];
      int *temp_ptr_statemt_136_190;
      temp_ptr_statemt_136_190 = &statemt[19];
      pushDbg( *temp_ptr_statemt_136_190,2190);
       *temp_ptr_statemt_136_190 = temp;
      temp = Sbox[statemt[11] >> 4][statemt[11] & 0xf];
      pushDbg(temp,2192);
      int *temp_ptr_statemt_136_192;
      temp_ptr_statemt_136_192 = &statemt[11];
      pushDbg( *temp_ptr_statemt_136_192,2199);
       *temp_ptr_statemt_136_192 = Sbox[statemt[23] >> 4][statemt[23] & 0xf];
      int *temp_ptr_statemt_136_193;
      temp_ptr_statemt_136_193 = &statemt[23];
      pushDbg( *temp_ptr_statemt_136_193,2207);
       *temp_ptr_statemt_136_193 = temp;
      int *temp_ptr_statemt_136_195;
      temp_ptr_statemt_136_195 = &statemt[0];
      pushDbg( *temp_ptr_statemt_136_195,2209);
       *temp_ptr_statemt_136_195 = Sbox[statemt[0] >> 4][statemt[0] & 0xf];
      int *temp_ptr_statemt_136_196;
      temp_ptr_statemt_136_196 = &statemt[4];
      pushDbg( *temp_ptr_statemt_136_196,2217);
       *temp_ptr_statemt_136_196 = Sbox[statemt[4] >> 4][statemt[4] & 0xf];
      int *temp_ptr_statemt_136_197;
      temp_ptr_statemt_136_197 = &statemt[8];
      pushDbg( *temp_ptr_statemt_136_197,2225);
       *temp_ptr_statemt_136_197 = Sbox[statemt[8] >> 4][statemt[8] & 0xf];
      int *temp_ptr_statemt_136_198;
      temp_ptr_statemt_136_198 = &statemt[12];
      pushDbg( *temp_ptr_statemt_136_198,2233);
       *temp_ptr_statemt_136_198 = Sbox[statemt[12] >> 4][statemt[12] & 0xf];
      int *temp_ptr_statemt_136_199;
      temp_ptr_statemt_136_199 = &statemt[16];
      pushDbg( *temp_ptr_statemt_136_199,2241);
       *temp_ptr_statemt_136_199 = Sbox[statemt[16] >> 4][statemt[16] & 0xf];
      int *temp_ptr_statemt_136_200;
      temp_ptr_statemt_136_200 = &statemt[20];
      pushDbg( *temp_ptr_statemt_136_200,2249);
       *temp_ptr_statemt_136_200 = Sbox[statemt[20] >> 4][statemt[20] & 0xf];
      break; 
    }
    case 8:
{
      temp = Sbox[statemt[1] >> 4][statemt[1] & 0xf];
      pushDbg(temp,2257);
      int *temp_ptr_statemt_136_204;
      temp_ptr_statemt_136_204 = &statemt[1];
      pushDbg( *temp_ptr_statemt_136_204,2264);
       *temp_ptr_statemt_136_204 = Sbox[statemt[5] >> 4][statemt[5] & 0xf];
      int *temp_ptr_statemt_136_205;
      temp_ptr_statemt_136_205 = &statemt[5];
      pushDbg( *temp_ptr_statemt_136_205,2272);
       *temp_ptr_statemt_136_205 = Sbox[statemt[9] >> 4][statemt[9] & 0xf];
      int *temp_ptr_statemt_136_206;
      temp_ptr_statemt_136_206 = &statemt[9];
      pushDbg( *temp_ptr_statemt_136_206,2280);
       *temp_ptr_statemt_136_206 = Sbox[statemt[13] >> 4][statemt[13] & 0xf];
      int *temp_ptr_statemt_136_207;
      temp_ptr_statemt_136_207 = &statemt[13];
      pushDbg( *temp_ptr_statemt_136_207,2288);
       *temp_ptr_statemt_136_207 = Sbox[statemt[17] >> 4][statemt[17] & 0xf];
      int *temp_ptr_statemt_136_208;
      temp_ptr_statemt_136_208 = &statemt[17];
      pushDbg( *temp_ptr_statemt_136_208,2296);
       *temp_ptr_statemt_136_208 = Sbox[statemt[21] >> 4][statemt[21] & 0xf];
      int *temp_ptr_statemt_136_209;
      temp_ptr_statemt_136_209 = &statemt[21];
      pushDbg( *temp_ptr_statemt_136_209,2304);
       *temp_ptr_statemt_136_209 = Sbox[statemt[25] >> 4][statemt[25] & 0xf];
      int *temp_ptr_statemt_136_210;
      temp_ptr_statemt_136_210 = &statemt[25];
      pushDbg( *temp_ptr_statemt_136_210,2312);
       *temp_ptr_statemt_136_210 = Sbox[statemt[29] >> 4][statemt[29] & 0xf];
      int *temp_ptr_statemt_136_211;
      temp_ptr_statemt_136_211 = &statemt[29];
      pushDbg( *temp_ptr_statemt_136_211,2320);
       *temp_ptr_statemt_136_211 = temp;
      temp = Sbox[statemt[2] >> 4][statemt[2] & 0xf];
      pushDbg(temp,2322);
      int *temp_ptr_statemt_136_214;
      temp_ptr_statemt_136_214 = &statemt[2];
      pushDbg( *temp_ptr_statemt_136_214,2329);
       *temp_ptr_statemt_136_214 = Sbox[statemt[14] >> 4][statemt[14] & 0xf];
      int *temp_ptr_statemt_136_215;
      temp_ptr_statemt_136_215 = &statemt[14];
      pushDbg( *temp_ptr_statemt_136_215,2337);
       *temp_ptr_statemt_136_215 = Sbox[statemt[26] >> 4][statemt[26] & 0xf];
      int *temp_ptr_statemt_136_216;
      temp_ptr_statemt_136_216 = &statemt[26];
      pushDbg( *temp_ptr_statemt_136_216,2345);
       *temp_ptr_statemt_136_216 = Sbox[statemt[6] >> 4][statemt[6] & 0xf];
      int *temp_ptr_statemt_136_217;
      temp_ptr_statemt_136_217 = &statemt[6];
      pushDbg( *temp_ptr_statemt_136_217,2353);
       *temp_ptr_statemt_136_217 = Sbox[statemt[18] >> 4][statemt[18] & 0xf];
      int *temp_ptr_statemt_136_218;
      temp_ptr_statemt_136_218 = &statemt[18];
      pushDbg( *temp_ptr_statemt_136_218,2361);
       *temp_ptr_statemt_136_218 = Sbox[statemt[30] >> 4][statemt[30] & 0xf];
      int *temp_ptr_statemt_136_219;
      temp_ptr_statemt_136_219 = &statemt[30];
      pushDbg( *temp_ptr_statemt_136_219,2369);
       *temp_ptr_statemt_136_219 = Sbox[statemt[10] >> 4][statemt[10] & 0xf];
      int *temp_ptr_statemt_136_220;
      temp_ptr_statemt_136_220 = &statemt[10];
      pushDbg( *temp_ptr_statemt_136_220,2377);
       *temp_ptr_statemt_136_220 = Sbox[statemt[22] >> 4][statemt[22] & 0xf];
      int *temp_ptr_statemt_136_221;
      temp_ptr_statemt_136_221 = &statemt[22];
      pushDbg( *temp_ptr_statemt_136_221,2385);
       *temp_ptr_statemt_136_221 = temp;
      temp = Sbox[statemt[3] >> 4][statemt[3] & 0xf];
      pushDbg(temp,2387);
      int *temp_ptr_statemt_136_224;
      temp_ptr_statemt_136_224 = &statemt[3];
      pushDbg( *temp_ptr_statemt_136_224,2394);
       *temp_ptr_statemt_136_224 = Sbox[statemt[19] >> 4][statemt[19] & 0xf];
      int *temp_ptr_statemt_136_225;
      temp_ptr_statemt_136_225 = &statemt[19];
      pushDbg( *temp_ptr_statemt_136_225,2402);
       *temp_ptr_statemt_136_225 = temp;
      temp = Sbox[statemt[7] >> 4][statemt[7] & 0xf];
      pushDbg(temp,2404);
      int *temp_ptr_statemt_136_227;
      temp_ptr_statemt_136_227 = &statemt[7];
      pushDbg( *temp_ptr_statemt_136_227,2411);
       *temp_ptr_statemt_136_227 = Sbox[statemt[23] >> 4][statemt[23] & 0xf];
      int *temp_ptr_statemt_136_228;
      temp_ptr_statemt_136_228 = &statemt[23];
      pushDbg( *temp_ptr_statemt_136_228,2419);
       *temp_ptr_statemt_136_228 = temp;
      temp = Sbox[statemt[11] >> 4][statemt[11] & 0xf];
      pushDbg(temp,2421);
      int *temp_ptr_statemt_136_230;
      temp_ptr_statemt_136_230 = &statemt[11];
      pushDbg( *temp_ptr_statemt_136_230,2428);
       *temp_ptr_statemt_136_230 = Sbox[statemt[27] >> 4][statemt[27] & 0xf];
      int *temp_ptr_statemt_136_231;
      temp_ptr_statemt_136_231 = &statemt[27];
      pushDbg( *temp_ptr_statemt_136_231,2436);
       *temp_ptr_statemt_136_231 = temp;
      temp = Sbox[statemt[15] >> 4][statemt[15] & 0xf];
      pushDbg(temp,2438);
      int *temp_ptr_statemt_136_233;
      temp_ptr_statemt_136_233 = &statemt[15];
      pushDbg( *temp_ptr_statemt_136_233,2445);
       *temp_ptr_statemt_136_233 = Sbox[statemt[31] >> 4][statemt[31] & 0xf];
      int *temp_ptr_statemt_136_234;
      temp_ptr_statemt_136_234 = &statemt[31];
      pushDbg( *temp_ptr_statemt_136_234,2453);
       *temp_ptr_statemt_136_234 = temp;
      int *temp_ptr_statemt_136_236;
      temp_ptr_statemt_136_236 = &statemt[0];
      pushDbg( *temp_ptr_statemt_136_236,2455);
       *temp_ptr_statemt_136_236 = Sbox[statemt[0] >> 4][statemt[0] & 0xf];
      int *temp_ptr_statemt_136_237;
      temp_ptr_statemt_136_237 = &statemt[4];
      pushDbg( *temp_ptr_statemt_136_237,2463);
       *temp_ptr_statemt_136_237 = Sbox[statemt[4] >> 4][statemt[4] & 0xf];
      int *temp_ptr_statemt_136_238;
      temp_ptr_statemt_136_238 = &statemt[8];
      pushDbg( *temp_ptr_statemt_136_238,2471);
       *temp_ptr_statemt_136_238 = Sbox[statemt[8] >> 4][statemt[8] & 0xf];
      int *temp_ptr_statemt_136_239;
      temp_ptr_statemt_136_239 = &statemt[12];
      pushDbg( *temp_ptr_statemt_136_239,2479);
       *temp_ptr_statemt_136_239 = Sbox[statemt[12] >> 4][statemt[12] & 0xf];
      int *temp_ptr_statemt_136_240;
      temp_ptr_statemt_136_240 = &statemt[16];
      pushDbg( *temp_ptr_statemt_136_240,2487);
       *temp_ptr_statemt_136_240 = Sbox[statemt[16] >> 4][statemt[16] & 0xf];
      int *temp_ptr_statemt_136_241;
      temp_ptr_statemt_136_241 = &statemt[20];
      pushDbg( *temp_ptr_statemt_136_241,2495);
       *temp_ptr_statemt_136_241 = Sbox[statemt[20] >> 4][statemt[20] & 0xf];
      int *temp_ptr_statemt_136_242;
      temp_ptr_statemt_136_242 = &statemt[24];
      pushDbg( *temp_ptr_statemt_136_242,2503);
       *temp_ptr_statemt_136_242 = Sbox[statemt[24] >> 4][statemt[24] & 0xf];
      int *temp_ptr_statemt_136_243;
      temp_ptr_statemt_136_243 = &statemt[28];
      pushDbg( *temp_ptr_statemt_136_243,2511);
       *temp_ptr_statemt_136_243 = Sbox[statemt[28] >> 4][statemt[28] & 0xf];
      break; 
    }
  }
}

int SubByte(int in)
{
  return Sbox[in / 16][in % 16];
}
/* ********* InversShiftRow & ByteSub ********* */

void InversShiftRow_ByteSub(int statemt[32],int nb)
{
  int temp;
  switch(nb){
    case 4:
{
      temp = invSbox[statemt[13] >> 4][statemt[13] & 0xf];
      pushDbg(temp,2523);
      int *temp_ptr_statemt_256_264;
      temp_ptr_statemt_256_264 = &statemt[13];
      pushDbg( *temp_ptr_statemt_256_264,2530);
       *temp_ptr_statemt_256_264 = invSbox[statemt[9] >> 4][statemt[9] & 0xf];
      int *temp_ptr_statemt_256_265;
      temp_ptr_statemt_256_265 = &statemt[9];
      pushDbg( *temp_ptr_statemt_256_265,2538);
       *temp_ptr_statemt_256_265 = invSbox[statemt[5] >> 4][statemt[5] & 0xf];
      int *temp_ptr_statemt_256_266;
      temp_ptr_statemt_256_266 = &statemt[5];
      pushDbg( *temp_ptr_statemt_256_266,2546);
       *temp_ptr_statemt_256_266 = invSbox[statemt[1] >> 4][statemt[1] & 0xf];
      int *temp_ptr_statemt_256_267;
      temp_ptr_statemt_256_267 = &statemt[1];
      pushDbg( *temp_ptr_statemt_256_267,2554);
       *temp_ptr_statemt_256_267 = temp;
      temp = invSbox[statemt[14] >> 4][statemt[14] & 0xf];
      pushDbg(temp,2556);
      int *temp_ptr_statemt_256_270;
      temp_ptr_statemt_256_270 = &statemt[14];
      pushDbg( *temp_ptr_statemt_256_270,2563);
       *temp_ptr_statemt_256_270 = invSbox[statemt[6] >> 4][statemt[6] & 0xf];
      int *temp_ptr_statemt_256_271;
      temp_ptr_statemt_256_271 = &statemt[6];
      pushDbg( *temp_ptr_statemt_256_271,2571);
       *temp_ptr_statemt_256_271 = temp;
      temp = invSbox[statemt[2] >> 4][statemt[2] & 0xf];
      pushDbg(temp,2573);
      int *temp_ptr_statemt_256_273;
      temp_ptr_statemt_256_273 = &statemt[2];
      pushDbg( *temp_ptr_statemt_256_273,2580);
       *temp_ptr_statemt_256_273 = invSbox[statemt[10] >> 4][statemt[10] & 0xf];
      int *temp_ptr_statemt_256_274;
      temp_ptr_statemt_256_274 = &statemt[10];
      pushDbg( *temp_ptr_statemt_256_274,2588);
       *temp_ptr_statemt_256_274 = temp;
      temp = invSbox[statemt[15] >> 4][statemt[15] & 0xf];
      pushDbg(temp,2590);
      int *temp_ptr_statemt_256_277;
      temp_ptr_statemt_256_277 = &statemt[15];
      pushDbg( *temp_ptr_statemt_256_277,2597);
       *temp_ptr_statemt_256_277 = invSbox[statemt[3] >> 4][statemt[3] & 0xf];
      int *temp_ptr_statemt_256_278;
      temp_ptr_statemt_256_278 = &statemt[3];
      pushDbg( *temp_ptr_statemt_256_278,2605);
       *temp_ptr_statemt_256_278 = invSbox[statemt[7] >> 4][statemt[7] & 0xf];
      int *temp_ptr_statemt_256_279;
      temp_ptr_statemt_256_279 = &statemt[7];
      pushDbg( *temp_ptr_statemt_256_279,2613);
       *temp_ptr_statemt_256_279 = invSbox[statemt[11] >> 4][statemt[11] & 0xf];
      int *temp_ptr_statemt_256_280;
      temp_ptr_statemt_256_280 = &statemt[11];
      pushDbg( *temp_ptr_statemt_256_280,2621);
       *temp_ptr_statemt_256_280 = temp;
      int *temp_ptr_statemt_256_282;
      temp_ptr_statemt_256_282 = &statemt[0];
      pushDbg( *temp_ptr_statemt_256_282,2623);
       *temp_ptr_statemt_256_282 = invSbox[statemt[0] >> 4][statemt[0] & 0xf];
      int *temp_ptr_statemt_256_283;
      temp_ptr_statemt_256_283 = &statemt[4];
      pushDbg( *temp_ptr_statemt_256_283,2631);
       *temp_ptr_statemt_256_283 = invSbox[statemt[4] >> 4][statemt[4] & 0xf];
      int *temp_ptr_statemt_256_284;
      temp_ptr_statemt_256_284 = &statemt[8];
      pushDbg( *temp_ptr_statemt_256_284,2639);
       *temp_ptr_statemt_256_284 = invSbox[statemt[8] >> 4][statemt[8] & 0xf];
      int *temp_ptr_statemt_256_285;
      temp_ptr_statemt_256_285 = &statemt[12];
      pushDbg( *temp_ptr_statemt_256_285,2647);
       *temp_ptr_statemt_256_285 = invSbox[statemt[12] >> 4][statemt[12] & 0xf];
      break; 
    }
    case 6:
{
      temp = invSbox[statemt[21] >> 4][statemt[21] & 0xf];
      pushDbg(temp,2655);
      int *temp_ptr_statemt_256_289;
      temp_ptr_statemt_256_289 = &statemt[21];
      pushDbg( *temp_ptr_statemt_256_289,2662);
       *temp_ptr_statemt_256_289 = invSbox[statemt[17] >> 4][statemt[17] & 0xf];
      int *temp_ptr_statemt_256_290;
      temp_ptr_statemt_256_290 = &statemt[17];
      pushDbg( *temp_ptr_statemt_256_290,2670);
       *temp_ptr_statemt_256_290 = invSbox[statemt[13] >> 4][statemt[13] & 0xf];
      int *temp_ptr_statemt_256_291;
      temp_ptr_statemt_256_291 = &statemt[13];
      pushDbg( *temp_ptr_statemt_256_291,2678);
       *temp_ptr_statemt_256_291 = invSbox[statemt[9] >> 4][statemt[9] & 0xf];
      int *temp_ptr_statemt_256_292;
      temp_ptr_statemt_256_292 = &statemt[9];
      pushDbg( *temp_ptr_statemt_256_292,2686);
       *temp_ptr_statemt_256_292 = invSbox[statemt[5] >> 4][statemt[5] & 0xf];
      int *temp_ptr_statemt_256_293;
      temp_ptr_statemt_256_293 = &statemt[5];
      pushDbg( *temp_ptr_statemt_256_293,2694);
       *temp_ptr_statemt_256_293 = invSbox[statemt[1] >> 4][statemt[1] & 0xf];
      int *temp_ptr_statemt_256_294;
      temp_ptr_statemt_256_294 = &statemt[1];
      pushDbg( *temp_ptr_statemt_256_294,2702);
       *temp_ptr_statemt_256_294 = temp;
      temp = invSbox[statemt[22] >> 4][statemt[22] & 0xf];
      pushDbg(temp,2704);
      int *temp_ptr_statemt_256_297;
      temp_ptr_statemt_256_297 = &statemt[22];
      pushDbg( *temp_ptr_statemt_256_297,2711);
       *temp_ptr_statemt_256_297 = invSbox[statemt[14] >> 4][statemt[14] & 0xf];
      int *temp_ptr_statemt_256_298;
      temp_ptr_statemt_256_298 = &statemt[14];
      pushDbg( *temp_ptr_statemt_256_298,2719);
       *temp_ptr_statemt_256_298 = invSbox[statemt[6] >> 4][statemt[6] & 0xf];
      int *temp_ptr_statemt_256_299;
      temp_ptr_statemt_256_299 = &statemt[6];
      pushDbg( *temp_ptr_statemt_256_299,2727);
       *temp_ptr_statemt_256_299 = temp;
      temp = invSbox[statemt[18] >> 4][statemt[18] & 0xf];
      pushDbg(temp,2729);
      int *temp_ptr_statemt_256_301;
      temp_ptr_statemt_256_301 = &statemt[18];
      pushDbg( *temp_ptr_statemt_256_301,2736);
       *temp_ptr_statemt_256_301 = invSbox[statemt[10] >> 4][statemt[10] & 0xf];
      int *temp_ptr_statemt_256_302;
      temp_ptr_statemt_256_302 = &statemt[10];
      pushDbg( *temp_ptr_statemt_256_302,2744);
       *temp_ptr_statemt_256_302 = invSbox[statemt[2] >> 4][statemt[2] & 0xf];
      int *temp_ptr_statemt_256_303;
      temp_ptr_statemt_256_303 = &statemt[2];
      pushDbg( *temp_ptr_statemt_256_303,2752);
       *temp_ptr_statemt_256_303 = temp;
      temp = invSbox[statemt[15] >> 4][statemt[15] & 0xf];
      pushDbg(temp,2754);
      int *temp_ptr_statemt_256_306;
      temp_ptr_statemt_256_306 = &statemt[15];
      pushDbg( *temp_ptr_statemt_256_306,2761);
       *temp_ptr_statemt_256_306 = invSbox[statemt[3] >> 4][statemt[3] & 0xf];
      int *temp_ptr_statemt_256_307;
      temp_ptr_statemt_256_307 = &statemt[3];
      pushDbg( *temp_ptr_statemt_256_307,2769);
       *temp_ptr_statemt_256_307 = temp;
      temp = invSbox[statemt[19] >> 4][statemt[19] & 0xf];
      pushDbg(temp,2771);
      int *temp_ptr_statemt_256_309;
      temp_ptr_statemt_256_309 = &statemt[19];
      pushDbg( *temp_ptr_statemt_256_309,2778);
       *temp_ptr_statemt_256_309 = invSbox[statemt[7] >> 4][statemt[7] & 0xf];
      int *temp_ptr_statemt_256_310;
      temp_ptr_statemt_256_310 = &statemt[7];
      pushDbg( *temp_ptr_statemt_256_310,2786);
       *temp_ptr_statemt_256_310 = temp;
      temp = invSbox[statemt[23] >> 4][statemt[23] & 0xf];
      pushDbg(temp,2788);
      int *temp_ptr_statemt_256_312;
      temp_ptr_statemt_256_312 = &statemt[23];
      pushDbg( *temp_ptr_statemt_256_312,2795);
       *temp_ptr_statemt_256_312 = invSbox[statemt[11] >> 4][statemt[11] & 0xf];
      int *temp_ptr_statemt_256_313;
      temp_ptr_statemt_256_313 = &statemt[11];
      pushDbg( *temp_ptr_statemt_256_313,2803);
       *temp_ptr_statemt_256_313 = temp;
      int *temp_ptr_statemt_256_315;
      temp_ptr_statemt_256_315 = &statemt[0];
      pushDbg( *temp_ptr_statemt_256_315,2805);
       *temp_ptr_statemt_256_315 = invSbox[statemt[0] >> 4][statemt[0] & 0xf];
      int *temp_ptr_statemt_256_316;
      temp_ptr_statemt_256_316 = &statemt[4];
      pushDbg( *temp_ptr_statemt_256_316,2813);
       *temp_ptr_statemt_256_316 = invSbox[statemt[4] >> 4][statemt[4] & 0xf];
      int *temp_ptr_statemt_256_317;
      temp_ptr_statemt_256_317 = &statemt[8];
      pushDbg( *temp_ptr_statemt_256_317,2821);
       *temp_ptr_statemt_256_317 = invSbox[statemt[8] >> 4][statemt[8] & 0xf];
      int *temp_ptr_statemt_256_318;
      temp_ptr_statemt_256_318 = &statemt[12];
      pushDbg( *temp_ptr_statemt_256_318,2829);
       *temp_ptr_statemt_256_318 = invSbox[statemt[12] >> 4][statemt[12] & 0xf];
      int *temp_ptr_statemt_256_319;
      temp_ptr_statemt_256_319 = &statemt[16];
      pushDbg( *temp_ptr_statemt_256_319,2837);
       *temp_ptr_statemt_256_319 = invSbox[statemt[16] >> 4][statemt[16] & 0xf];
      int *temp_ptr_statemt_256_320;
      temp_ptr_statemt_256_320 = &statemt[20];
      pushDbg( *temp_ptr_statemt_256_320,2845);
       *temp_ptr_statemt_256_320 = invSbox[statemt[20] >> 4][statemt[20] & 0xf];
      break; 
    }
    case 8:
{
      temp = invSbox[statemt[29] >> 4][statemt[29] & 0xf];
      pushDbg(temp,2853);
      int *temp_ptr_statemt_256_324;
      temp_ptr_statemt_256_324 = &statemt[29];
      pushDbg( *temp_ptr_statemt_256_324,2860);
       *temp_ptr_statemt_256_324 = invSbox[statemt[25] >> 4][statemt[25] & 0xf];
      int *temp_ptr_statemt_256_325;
      temp_ptr_statemt_256_325 = &statemt[25];
      pushDbg( *temp_ptr_statemt_256_325,2868);
       *temp_ptr_statemt_256_325 = invSbox[statemt[21] >> 4][statemt[21] & 0xf];
      int *temp_ptr_statemt_256_326;
      temp_ptr_statemt_256_326 = &statemt[21];
      pushDbg( *temp_ptr_statemt_256_326,2876);
       *temp_ptr_statemt_256_326 = invSbox[statemt[17] >> 4][statemt[17] & 0xf];
      int *temp_ptr_statemt_256_327;
      temp_ptr_statemt_256_327 = &statemt[17];
      pushDbg( *temp_ptr_statemt_256_327,2884);
       *temp_ptr_statemt_256_327 = invSbox[statemt[13] >> 4][statemt[13] & 0xf];
      int *temp_ptr_statemt_256_328;
      temp_ptr_statemt_256_328 = &statemt[13];
      pushDbg( *temp_ptr_statemt_256_328,2892);
       *temp_ptr_statemt_256_328 = invSbox[statemt[9] >> 4][statemt[9] & 0xf];
      int *temp_ptr_statemt_256_329;
      temp_ptr_statemt_256_329 = &statemt[9];
      pushDbg( *temp_ptr_statemt_256_329,2900);
       *temp_ptr_statemt_256_329 = invSbox[statemt[5] >> 4][statemt[5] & 0xf];
      int *temp_ptr_statemt_256_330;
      temp_ptr_statemt_256_330 = &statemt[5];
      pushDbg( *temp_ptr_statemt_256_330,2908);
       *temp_ptr_statemt_256_330 = invSbox[statemt[1] >> 4][statemt[1] & 0xf];
      int *temp_ptr_statemt_256_331;
      temp_ptr_statemt_256_331 = &statemt[1];
      pushDbg( *temp_ptr_statemt_256_331,2916);
       *temp_ptr_statemt_256_331 = temp;
      temp = invSbox[statemt[30] >> 4][statemt[30] & 0xf];
      pushDbg(temp,2918);
      int *temp_ptr_statemt_256_334;
      temp_ptr_statemt_256_334 = &statemt[30];
      pushDbg( *temp_ptr_statemt_256_334,2925);
       *temp_ptr_statemt_256_334 = invSbox[statemt[18] >> 4][statemt[18] & 0xf];
      int *temp_ptr_statemt_256_335;
      temp_ptr_statemt_256_335 = &statemt[18];
      pushDbg( *temp_ptr_statemt_256_335,2933);
       *temp_ptr_statemt_256_335 = invSbox[statemt[6] >> 4][statemt[6] & 0xf];
      int *temp_ptr_statemt_256_336;
      temp_ptr_statemt_256_336 = &statemt[6];
      pushDbg( *temp_ptr_statemt_256_336,2941);
       *temp_ptr_statemt_256_336 = invSbox[statemt[26] >> 4][statemt[26] & 0xf];
      int *temp_ptr_statemt_256_337;
      temp_ptr_statemt_256_337 = &statemt[26];
      pushDbg( *temp_ptr_statemt_256_337,2949);
       *temp_ptr_statemt_256_337 = invSbox[statemt[14] >> 4][statemt[14] & 0xf];
      int *temp_ptr_statemt_256_338;
      temp_ptr_statemt_256_338 = &statemt[14];
      pushDbg( *temp_ptr_statemt_256_338,2957);
       *temp_ptr_statemt_256_338 = invSbox[statemt[2] >> 4][statemt[2] & 0xf];
      int *temp_ptr_statemt_256_339;
      temp_ptr_statemt_256_339 = &statemt[2];
      pushDbg( *temp_ptr_statemt_256_339,2965);
       *temp_ptr_statemt_256_339 = invSbox[statemt[22] >> 4][statemt[22] & 0xf];
      int *temp_ptr_statemt_256_340;
      temp_ptr_statemt_256_340 = &statemt[22];
      pushDbg( *temp_ptr_statemt_256_340,2973);
       *temp_ptr_statemt_256_340 = invSbox[statemt[10] >> 4][statemt[10] & 0xf];
      int *temp_ptr_statemt_256_341;
      temp_ptr_statemt_256_341 = &statemt[10];
      pushDbg( *temp_ptr_statemt_256_341,2981);
       *temp_ptr_statemt_256_341 = temp;
      temp = invSbox[statemt[31] >> 4][statemt[31] & 0xf];
      pushDbg(temp,2983);
      int *temp_ptr_statemt_256_344;
      temp_ptr_statemt_256_344 = &statemt[31];
      pushDbg( *temp_ptr_statemt_256_344,2990);
       *temp_ptr_statemt_256_344 = invSbox[statemt[15] >> 4][statemt[15] & 0xf];
      int *temp_ptr_statemt_256_345;
      temp_ptr_statemt_256_345 = &statemt[15];
      pushDbg( *temp_ptr_statemt_256_345,2998);
       *temp_ptr_statemt_256_345 = temp;
      temp = invSbox[statemt[27] >> 4][statemt[27] & 0xf];
      pushDbg(temp,3000);
      int *temp_ptr_statemt_256_347;
      temp_ptr_statemt_256_347 = &statemt[27];
      pushDbg( *temp_ptr_statemt_256_347,3007);
       *temp_ptr_statemt_256_347 = invSbox[statemt[11] >> 4][statemt[11] & 0xf];
      int *temp_ptr_statemt_256_348;
      temp_ptr_statemt_256_348 = &statemt[11];
      pushDbg( *temp_ptr_statemt_256_348,3015);
       *temp_ptr_statemt_256_348 = temp;
      temp = invSbox[statemt[23] >> 4][statemt[23] & 0xf];
      pushDbg(temp,3017);
      int *temp_ptr_statemt_256_350;
      temp_ptr_statemt_256_350 = &statemt[23];
      pushDbg( *temp_ptr_statemt_256_350,3024);
       *temp_ptr_statemt_256_350 = invSbox[statemt[7] >> 4][statemt[7] & 0xf];
      int *temp_ptr_statemt_256_351;
      temp_ptr_statemt_256_351 = &statemt[7];
      pushDbg( *temp_ptr_statemt_256_351,3032);
       *temp_ptr_statemt_256_351 = temp;
      temp = invSbox[statemt[19] >> 4][statemt[19] & 0xf];
      pushDbg(temp,3034);
      int *temp_ptr_statemt_256_353;
      temp_ptr_statemt_256_353 = &statemt[19];
      pushDbg( *temp_ptr_statemt_256_353,3041);
       *temp_ptr_statemt_256_353 = invSbox[statemt[3] >> 4][statemt[3] & 0xf];
      int *temp_ptr_statemt_256_354;
      temp_ptr_statemt_256_354 = &statemt[3];
      pushDbg( *temp_ptr_statemt_256_354,3049);
       *temp_ptr_statemt_256_354 = temp;
      int *temp_ptr_statemt_256_356;
      temp_ptr_statemt_256_356 = &statemt[0];
      pushDbg( *temp_ptr_statemt_256_356,3051);
       *temp_ptr_statemt_256_356 = invSbox[statemt[0] >> 4][statemt[0] & 0xf];
      int *temp_ptr_statemt_256_357;
      temp_ptr_statemt_256_357 = &statemt[4];
      pushDbg( *temp_ptr_statemt_256_357,3059);
       *temp_ptr_statemt_256_357 = invSbox[statemt[4] >> 4][statemt[4] & 0xf];
      int *temp_ptr_statemt_256_358;
      temp_ptr_statemt_256_358 = &statemt[8];
      pushDbg( *temp_ptr_statemt_256_358,3067);
       *temp_ptr_statemt_256_358 = invSbox[statemt[8] >> 4][statemt[8] & 0xf];
      int *temp_ptr_statemt_256_359;
      temp_ptr_statemt_256_359 = &statemt[12];
      pushDbg( *temp_ptr_statemt_256_359,3075);
       *temp_ptr_statemt_256_359 = invSbox[statemt[12] >> 4][statemt[12] & 0xf];
      int *temp_ptr_statemt_256_360;
      temp_ptr_statemt_256_360 = &statemt[16];
      pushDbg( *temp_ptr_statemt_256_360,3083);
       *temp_ptr_statemt_256_360 = invSbox[statemt[16] >> 4][statemt[16] & 0xf];
      int *temp_ptr_statemt_256_361;
      temp_ptr_statemt_256_361 = &statemt[20];
      pushDbg( *temp_ptr_statemt_256_361,3091);
       *temp_ptr_statemt_256_361 = invSbox[statemt[20] >> 4][statemt[20] & 0xf];
      int *temp_ptr_statemt_256_362;
      temp_ptr_statemt_256_362 = &statemt[24];
      pushDbg( *temp_ptr_statemt_256_362,3099);
       *temp_ptr_statemt_256_362 = invSbox[statemt[24] >> 4][statemt[24] & 0xf];
      int *temp_ptr_statemt_256_363;
      temp_ptr_statemt_256_363 = &statemt[28];
      pushDbg( *temp_ptr_statemt_256_363,3107);
       *temp_ptr_statemt_256_363 = invSbox[statemt[28] >> 4][statemt[28] & 0xf];
      break; 
    }
  }
}
/* ******** MixColumn ********** */

int MixColumn_AddRoundKey(int statemt[32],int nb,int n)
{
  int ret[8 * 4];
  int j;
  register int x;
  for (j = 0; j < nb; ++j) {
    int *temp_ptr_ret_372_377;
    temp_ptr_ret_372_377 = &ret[j * 4];
    pushDbg( *temp_ptr_ret_372_377,3117);
     *temp_ptr_ret_372_377 = statemt[j * 4] << 1;
    if (ret[j * 4] >> 8 == 1) 
      ret[j * 4] ^= 283;
    x = statemt[1 + j * 4];
    pushDbg(x,3130);
    x ^= x << 1;
    if (x >> 8 == 1) 
      ret[j * 4] ^= x ^ 283;
     else 
      ret[j * 4] ^= x;
    ret[j * 4] ^= statemt[2 + j * 4] ^ statemt[3 + j * 4] ^ word[0][j + nb * n];
    int *temp_ptr_ret_372_389;
    temp_ptr_ret_372_389 = &ret[1 + j * 4];
    pushDbg( *temp_ptr_ret_372_389,3160);
     *temp_ptr_ret_372_389 = statemt[1 + j * 4] << 1;
    if (ret[1 + j * 4] >> 8 == 1) 
      ret[1 + j * 4] ^= 283;
    x = statemt[2 + j * 4];
    pushDbg(x,3177);
    x ^= x << 1;
    if (x >> 8 == 1) 
      ret[1 + j * 4] ^= x ^ 283;
     else 
      ret[1 + j * 4] ^= x;
    ret[1 + j * 4] ^= statemt[3 + j * 4] ^ statemt[j * 4] ^ word[1][j + nb * n];
    int *temp_ptr_ret_372_401;
    temp_ptr_ret_372_401 = &ret[2 + j * 4];
    pushDbg( *temp_ptr_ret_372_401,3209);
     *temp_ptr_ret_372_401 = statemt[2 + j * 4] << 1;
    if (ret[2 + j * 4] >> 8 == 1) 
      ret[2 + j * 4] ^= 283;
    x = statemt[3 + j * 4];
    pushDbg(x,3226);
    x ^= x << 1;
    if (x >> 8 == 1) 
      ret[2 + j * 4] ^= x ^ 283;
     else 
      ret[2 + j * 4] ^= x;
    ret[2 + j * 4] ^= statemt[j * 4] ^ statemt[1 + j * 4] ^ word[2][j + nb * n];
    int *temp_ptr_ret_372_413;
    temp_ptr_ret_372_413 = &ret[3 + j * 4];
    pushDbg( *temp_ptr_ret_372_413,3258);
     *temp_ptr_ret_372_413 = statemt[3 + j * 4] << 1;
    if (ret[3 + j * 4] >> 8 == 1) 
      ret[3 + j * 4] ^= 283;
    x = statemt[j * 4];
    pushDbg(x,3275);
    x ^= x << 1;
    if (x >> 8 == 1) 
      ret[3 + j * 4] ^= x ^ 283;
     else 
      ret[3 + j * 4] ^= x;
    ret[3 + j * 4] ^= statemt[1 + j * 4] ^ statemt[2 + j * 4] ^ word[3][j + nb * n];
  }
  for (j = 0; j < nb; ++j) {
    int *temp_ptr_statemt_370_427;
    temp_ptr_statemt_370_427 = &statemt[j * 4];
    pushDbg( *temp_ptr_statemt_370_427,3309);
     *temp_ptr_statemt_370_427 = ret[j * 4];
    int *temp_ptr_statemt_370_428;
    temp_ptr_statemt_370_428 = &statemt[1 + j * 4];
    pushDbg( *temp_ptr_statemt_370_428,3314);
     *temp_ptr_statemt_370_428 = ret[1 + j * 4];
    int *temp_ptr_statemt_370_429;
    temp_ptr_statemt_370_429 = &statemt[2 + j * 4];
    pushDbg( *temp_ptr_statemt_370_429,3321);
     *temp_ptr_statemt_370_429 = ret[2 + j * 4];
    int *temp_ptr_statemt_370_430;
    temp_ptr_statemt_370_430 = &statemt[3 + j * 4];
    pushDbg( *temp_ptr_statemt_370_430,3328);
     *temp_ptr_statemt_370_430 = ret[3 + j * 4];
  }
  return 0;
}
/* ******** InversMixColumn ********** */

int AddRoundKey_InversMixColumn(int statemt[32],int nb,int n)
{
  int ret[8 * 4];
  int i;
  int j;
  register int x;
  for (j = 0; j < nb; ++j) {
    statemt[j * 4] ^= word[0][j + nb * n];
    statemt[1 + j * 4] ^= word[1][j + nb * n];
    statemt[2 + j * 4] ^= word[2][j + nb * n];
    statemt[3 + j * 4] ^= word[3][j + nb * n];
  }
  for (j = 0; j < nb; ++j) 
    for (i = 0; i < 4; ++i) {
      x = statemt[i + j * 4] << 1;
      pushDbg(x,3372);
      if (x >> 8 == 1) 
        x ^= 283;
      x ^= statemt[i + j * 4];
      x = x << 1;
      pushDbg(x,3384);
      if (x >> 8 == 1) 
        x ^= 283;
      x ^= statemt[i + j * 4];
      x = x << 1;
      pushDbg(x,3393);
      if (x >> 8 == 1) 
        x ^= 283;
      int *temp_ptr_ret_439_463;
      temp_ptr_ret_439_463 = &ret[i + j * 4];
      pushDbg( *temp_ptr_ret_439_463,3398);
       *temp_ptr_ret_439_463 = x;
      x = statemt[(i + 1) % 4 + j * 4] << 1;
      pushDbg(x,3402);
      if (x >> 8 == 1) 
        x ^= 283;
      x = x << 1;
      pushDbg(x,3412);
      if (x >> 8 == 1) 
        x ^= 283;
      x ^= statemt[(i + 1) % 4 + j * 4];
      x = x << 1;
      pushDbg(x,3423);
      if (x >> 8 == 1) 
        x ^= 283;
      x ^= statemt[(i + 1) % 4 + j * 4];
      ret[i + j * 4] ^= x;
      x = statemt[(i + 2) % 4 + j * 4] << 1;
      pushDbg(x,3438);
      if (x >> 8 == 1) 
        x ^= 283;
      x ^= statemt[(i + 2) % 4 + j * 4];
      x = x << 1;
      pushDbg(x,3454);
      if (x >> 8 == 1) 
        x ^= 283;
      x = x << 1;
      pushDbg(x,3459);
      if (x >> 8 == 1) 
        x ^= 283;
      x ^= statemt[(i + 2) % 4 + j * 4];
      ret[i + j * 4] ^= x;
      x = statemt[(i + 3) % 4 + j * 4] << 1;
      pushDbg(x,3474);
      if (x >> 8 == 1) 
        x ^= 283;
      x = x << 1;
      pushDbg(x,3484);
      if (x >> 8 == 1) 
        x ^= 283;
      x = x << 1;
      pushDbg(x,3489);
      if (x >> 8 == 1) 
        x ^= 283;
      x ^= statemt[(i + 3) % 4 + j * 4];
      ret[i + j * 4] ^= x;
    }
  for (i = 0; i < nb; ++i) {
    int *temp_ptr_statemt_437_505;
    temp_ptr_statemt_437_505 = &statemt[i * 4];
    pushDbg( *temp_ptr_statemt_437_505,3506);
     *temp_ptr_statemt_437_505 = ret[i * 4];
    int *temp_ptr_statemt_437_506;
    temp_ptr_statemt_437_506 = &statemt[1 + i * 4];
    pushDbg( *temp_ptr_statemt_437_506,3511);
     *temp_ptr_statemt_437_506 = ret[1 + i * 4];
    int *temp_ptr_statemt_437_507;
    temp_ptr_statemt_437_507 = &statemt[2 + i * 4];
    pushDbg( *temp_ptr_statemt_437_507,3518);
     *temp_ptr_statemt_437_507 = ret[2 + i * 4];
    int *temp_ptr_statemt_437_508;
    temp_ptr_statemt_437_508 = &statemt[3 + i * 4];
    pushDbg( *temp_ptr_statemt_437_508,3525);
     *temp_ptr_statemt_437_508 = ret[3 + i * 4];
  }
  return 0;
}
/* ******** AddRoundKey ********** */

int AddRoundKey(int statemt[32],int type,int n)
{
  int j;
  int nb;
  switch(type){
    case 128128:
{
    }
    case 192128:
{
    }
    case 256128:
{
      nb = 4;
      break; 
    }
    case 128192:
{
    }
    case 192192:
{
    }
    case 256192:
{
      nb = 6;
      break; 
    }
    case 128256:
{
    }
    case 192256:
{
    }
    case 256256:
{
      nb = 8;
      break; 
    }
  }
  for (j = 0; j < nb; ++j) {
    statemt[j * 4] ^= word[0][j + nb * n];
    statemt[1 + j * 4] ^= word[1][j + nb * n];
    statemt[2 + j * 4] ^= word[2][j + nb * n];
    statemt[3 + j * 4] ^= word[3][j + nb * n];
  }
  return 0;
}
