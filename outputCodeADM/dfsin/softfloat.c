/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*============================================================================
This C source file is part of the SoftFloat IEC/IEEE Floating-point Arithmetic
Package, Release 2b.
Written by John R. Hauser.  This work was made possible in part by the
International Computer Science Institute, located at Suite 600, 1947 Center
Street, Berkeley, California 94704.  Funding was partially provided by the
National Science Foundation under grant MIP-9311980.  The original version
of this code was written as part of a project to build a fixed-point vector
processor in collaboration with the University of California at Berkeley,
overseen by Profs. Nelson Morgan and John Wawrzynek.  More information
is available through the Web page `http://www.cs.berkeley.edu/~jhauser/
arithmetic/SoftFloat.html'.
THIS SOFTWARE IS DISTRIBUTED AS IS, FOR FREE.  Although reasonable effort has
been made to avoid it, THIS SOFTWARE MAY CONTAIN FAULTS THAT WILL AT TIMES
RESULT IN INCORRECT BEHAVIOR.  USE OF THIS SOFTWARE IS RESTRICTED TO PERSONS
AND ORGANIZATIONS WHO CAN AND WILL TAKE FULL RESPONSIBILITY FOR ALL LOSSES,
COSTS, OR OTHER PROBLEMS THEY INCUR DUE TO THE SOFTWARE, AND WHO FURTHERMORE
EFFECTIVELY INDEMNIFY JOHN HAUSER AND THE INTERNATIONAL COMPUTER SCIENCE
INSTITUTE (possibly via similar legal warning) AGAINST ALL LOSSES, COSTS, OR
OTHER PROBLEMS INCURRED BY THEIR CUSTOMERS AND CLIENTS DUE TO THE SOFTWARE.
Derivative works are acceptable, even for commercial purposes, so long as
(1) the source code for the derivative work includes prominent notice that
the work is derivative, and (2) the source code includes prominent notice with
these four paragraphs for those parts of this code that are retained.
=============================================================================*/
#include "milieu.h"
#include "softfloat.h"
/*----------------------------------------------------------------------------
| Floating-point rounding mode, extended double-precision rounding precision,
| and exception flags.
*----------------------------------------------------------------------------*/
int8 float_rounding_mode = 0;
int8 float_exception_flags = 0;
/*----------------------------------------------------------------------------
| Primitive arithmetic functions, including multi-word arithmetic, and
| division and square root approximations.  (Can be specialized to target if
| desired.)
*----------------------------------------------------------------------------*/
#include "softfloat-macros"
/*----------------------------------------------------------------------------
| Functions and definitions to determine:  (1) whether tininess for underflow
| is detected before or after rounding by default, (2) what (if anything)
| happens when exceptions are raised, (3) how signaling NaNs are distinguished
| from quiet NaNs, (4) the default generated quiet NaNs, and (5) how NaNs
| are propagated from function inputs to output.  These details are target-
| specific.
*----------------------------------------------------------------------------*/
#include "softfloat-specialize"
/*----------------------------------------------------------------------------
| Returns the fraction bits of the double-precision floating-point value `a'.
*----------------------------------------------------------------------------*/

bits64 extractFloat64Frac(float64 a)
{
  return a & 0x000FFFFFFFFFFFFFLL;
}
/*----------------------------------------------------------------------------
| Returns the exponent bits of the double-precision floating-point value `a'.
*----------------------------------------------------------------------------*/

int16 extractFloat64Exp(float64 a)
{
  return (a >> 52 & 0x7FF);
}
/*----------------------------------------------------------------------------
| Returns the sign bit of the double-precision floating-point value `a'.
*----------------------------------------------------------------------------*/

flag extractFloat64Sign(float64 a)
{
  return (a >> 63);
}
/*----------------------------------------------------------------------------
| Normalizes the subnormal double-precision floating-point value represented
| by the denormalized significand `aSig'.  The normalized exponent and
| significand are stored at the locations pointed to by `zExpPtr' and
| `zSigPtr', respectively.
*----------------------------------------------------------------------------*/

static void normalizeFloat64Subnormal(bits64 aSig,int16 *zExpPtr,bits64 *zSigPtr)
{
  int8 shiftCount;
  shiftCount = countLeadingZeros64(aSig) - 11;
  pushDbg(shiftCount,506);
  bits64 *temp_ptr_zSigPtr_121_126;
  temp_ptr_zSigPtr_121_126 = zSigPtr;
  pushDbgLong( *temp_ptr_zSigPtr_121_126,508);
   *temp_ptr_zSigPtr_121_126 = aSig << shiftCount;
  int16 *temp_ptr_zExpPtr_121_127;
  temp_ptr_zExpPtr_121_127 = zExpPtr;
  pushDbg( *temp_ptr_zExpPtr_121_127,510);
   *temp_ptr_zExpPtr_121_127 = 1 - shiftCount;
}
/*----------------------------------------------------------------------------
| Packs the sign `zSign', exponent `zExp', and significand `zSig' into a
| double-precision floating-point value, returning the result.  After being
| shifted into the proper positions, the three fields are simply added
| together to form the result.  This means that any integer portion of `zSig'
| will be added into the exponent.  Since a properly normalized significand
| will have an integer portion equal to 1, the `zExp' input should be 1 less
| than the desired result exponent whenever `zSig' is a complete, normalized
| significand.
*----------------------------------------------------------------------------*/

float64 packFloat64(flag zSign,int16 zExp,bits64 zSig)
{
  return (((bits64 )zSign) << 63) + (((bits64 )zExp) << 52) + zSig;
}
/*----------------------------------------------------------------------------
| Takes an abstract floating-point value having sign `zSign', exponent `zExp',
| and significand `zSig', and returns the proper double-precision floating-
| point value corresponding to the abstract input.  Ordinarily, the abstract
| value is simply rounded and packed into the double-precision format, with
| the inexact exception raised if the abstract input cannot be represented
| exactly.  However, if the abstract value is too large, the overflow and
| inexact exceptions are raised and an infinity or maximal finite value is
| returned.  If the abstract value is too small, the input value is rounded
| to a subnormal number, and the underflow and inexact exceptions are raised
| if the abstract input cannot be represented exactly as a subnormal double-
| precision floating-point number.
|     The input significand `zSig' has its binary point between bits 62
| and 61, which is 10 bits to the left of the usual location.  This shifted
| significand must be normalized or smaller.  If `zSig' is not normalized,
| `zExp' must be 0; in that case, the result returned is a subnormal number,
| and it must not require rounding.  In the usual case that `zSig' is
| normalized, `zExp' must be 1 less than the ``true'' floating-point exponent.
| The handling of underflow and overflow follows the IEC/IEEE Standard for
| Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

static float64 roundAndPackFloat64(flag zSign,int16 zExp,bits64 zSig)
{
  int8 roundingMode;
  flag roundNearestEven;
  flag isTiny;
  int16 roundIncrement;
  int16 roundBits;
  roundingMode = float_rounding_mode;
  pushDbg(roundingMode,516);
  roundNearestEven = roundingMode == 0;
  pushDbg(roundNearestEven,517);
  roundIncrement = 0x200;
  if (!roundNearestEven) {
    if (roundingMode == 1) {
      roundIncrement = 0;
    }
     else {
      roundIncrement = 0x3FF;
      if (zSign) {
        if (roundingMode == 2) 
          roundIncrement = 0;
      }
       else {
        if (roundingMode == 3) 
          roundIncrement = 0;
      }
    }
  }
  roundBits = (zSig & 0x3FF);
  pushDbg(roundBits,527);
  if (0x7FD <= ((bits16 )zExp)) {
    if (0x7FD < zExp || zExp == 0x7FD && ((sbits64 )(zSig + roundIncrement)) < 0) {
      float_raise(8 | 1);
      return packFloat64(zSign,0x7FF,0) - (roundIncrement == 0);
    }
    if (zExp < 0) {
      isTiny = 1 == 1 || zExp < - 1 || zSig + roundIncrement < 0x8000000000000000LL;
      pushDbg(isTiny,540);
      shift64RightJamming(zSig,-zExp,&zSig);
      zExp = 0;
      roundBits = (zSig & 0x3FF);
      pushDbg(roundBits,548);
      if (isTiny && roundBits) 
        float_raise(4);
    }
  }
  if (roundBits) 
    float_exception_flags |= 1;
  zSig = zSig + roundIncrement >> 10;
  pushDbgLong(zSig,552);
  zSig &= (~((roundBits ^ 0x200) == 0 & roundNearestEven));
  if (zSig == 0) 
    zExp = 0;
  return packFloat64(zSign,zExp,zSig);
}
/*----------------------------------------------------------------------------
| Takes an abstract floating-point value having sign `zSign', exponent `zExp',
| and significand `zSig', and returns the proper double-precision floating-
| point value corresponding to the abstract input.  This routine is just like
| `roundAndPackFloat64' except that `zSig' does not have to be normalized.
| Bit 63 of `zSig' must be zero, and `zExp' must be 1 less than the ``true''
| floating-point exponent.
*----------------------------------------------------------------------------*/

static float64 normalizeRoundAndPackFloat64(flag zSign,int16 zExp,bits64 zSig)
{
  int8 shiftCount;
  shiftCount = countLeadingZeros64(zSig) - 1;
  pushDbg(shiftCount,561);
  return roundAndPackFloat64(zSign,zExp - shiftCount,zSig << shiftCount);
}
/*----------------------------------------------------------------------------
| Returns the result of converting the 32-bit two's complement integer `a'
| to the double-precision floating-point format.  The conversion is performed
| according to the IEC/IEEE Standard for Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

float64 int32_to_float64(int32 a)
{
  flag zSign;
  uint32 absA;
  int8 shiftCount;
  bits64 zSig;
  if (a == 0) 
    return 0;
  zSign = a < 0;
  pushDbg(zSign,566);
  absA = ((zSign?-a : a));
  pushDbg(absA,568);
  shiftCount = countLeadingZeros32(absA) + 21;
  pushDbg(shiftCount,569);
  zSig = absA;
  pushDbgLong(zSig,571);
  return packFloat64(zSign,0x432 - shiftCount,zSig << shiftCount);
}
/*----------------------------------------------------------------------------
| Returns the result of adding the absolute values of the double-precision
| floating-point values `a' and `b'.  If `zSign' is 1, the sum is negated
| before being returned.  `zSign' is ignored if the result is a NaN.
| The addition is performed according to the IEC/IEEE Standard for Binary
| Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

static float64 addFloat64Sigs(float64 a,float64 b,flag zSign)
{
  int16 aExp;
  int16 bExp;
  int16 zExp;
  bits64 aSig;
  bits64 bSig;
  bits64 zSig;
  int16 expDiff;
  aSig = extractFloat64Frac(a);
  pushDbgLong(aSig,574);
  aExp = extractFloat64Exp(a);
  pushDbg(aExp,575);
  bSig = extractFloat64Frac(b);
  pushDbgLong(bSig,576);
  bExp = extractFloat64Exp(b);
  pushDbg(bExp,577);
  expDiff = aExp - bExp;
  pushDbg(expDiff,578);
  aSig <<= 9;
  bSig <<= 9;
  if (0 < expDiff) {
    if (aExp == 0x7FF) {
      if (aSig) 
        return propagateFloat64NaN(a,b);
      return a;
    }
    if (bExp == 0) {
      --expDiff;
      pushDbg(expDiff,1008);
    }
     else 
      bSig |= 0x2000000000000000LL;
    shift64RightJamming(bSig,expDiff,&bSig);
    zExp = aExp;
    pushDbg(zExp,586);
  }
   else if (expDiff < 0) {
    if (bExp == 0x7FF) {
      if (bSig) 
        return propagateFloat64NaN(a,b);
      return packFloat64(zSign,0x7FF,0);
    }
    if (aExp == 0) {
      ++expDiff;
      pushDbg(expDiff,1012);
    }
     else {
      aSig |= 0x2000000000000000LL;
    }
    shift64RightJamming(aSig,-expDiff,&aSig);
    zExp = bExp;
    pushDbg(zExp,591);
  }
   else {
    if (aExp == 0x7FF) {
      if (aSig | bSig) 
        return propagateFloat64NaN(a,b);
      return a;
    }
    if (aExp == 0) 
      return packFloat64(zSign,0,aSig + bSig >> 9);
    zSig = 0x4000000000000000LL + aSig + bSig;
    pushDbgLong(zSig,597);
    zExp = aExp;
    pushDbg(zExp,600);
    goto roundAndPack;
  }
  aSig |= 0x2000000000000000LL;
  zSig = aSig + bSig << 1;
  pushDbgLong(zSig,602);
  --zExp;
  pushDbg(zExp,1018);
  if (((sbits64 )zSig) < 0) {
    zSig = aSig + bSig;
    pushDbgLong(zSig,606);
    ++zExp;
    pushDbg(zExp,1021);
  }
  roundAndPack:
  return roundAndPackFloat64(zSign,zExp,zSig);
}
/*----------------------------------------------------------------------------
| Returns the result of subtracting the absolute values of the double-
| precision floating-point values `a' and `b'.  If `zSign' is 1, the
| difference is negated before being returned.  `zSign' is ignored if the
| result is a NaN.  The subtraction is performed according to the IEC/IEEE
| Standard for Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

static float64 subFloat64Sigs(float64 a,float64 b,flag zSign)
{
  int16 aExp;
  int16 bExp;
  int16 zExp;
  bits64 aSig;
  bits64 bSig;
  bits64 zSig;
  int16 expDiff;
  aSig = extractFloat64Frac(a);
  pushDbgLong(aSig,608);
  aExp = extractFloat64Exp(a);
  pushDbg(aExp,609);
  bSig = extractFloat64Frac(b);
  pushDbgLong(bSig,610);
  bExp = extractFloat64Exp(b);
  pushDbg(bExp,611);
  expDiff = aExp - bExp;
  pushDbg(expDiff,612);
  aSig <<= 10;
  bSig <<= 10;
  if (0 < expDiff) 
    goto aExpBigger;
  if (expDiff < 0) 
    goto bExpBigger;
  if (aExp == 0x7FF) {
    if (aSig | bSig) 
      return propagateFloat64NaN(a,b);
    float_raise(16);
    return 0x7FFFFFFFFFFFFFFFLL;
  }
  if (aExp == 0) {
    aExp = 1;
    bExp = 1;
  }
  if (bSig < aSig) 
    goto aBigger;
  if (aSig < bSig) 
    goto bBigger;
  return packFloat64(float_rounding_mode == 3,0,0);
  bExpBigger:
  if (bExp == 0x7FF) {
    if (bSig) 
      return propagateFloat64NaN(a,b);
    return packFloat64(zSign ^ 1,0x7FF,0);
  }
  if (aExp == 0) {
    ++expDiff;
    pushDbg(expDiff,1025);
  }
   else 
    aSig |= 0x4000000000000000LL;
  shift64RightJamming(aSig,-expDiff,&aSig);
  bSig |= 0x4000000000000000LL;
  bBigger:
  zSig = bSig - aSig;
  pushDbgLong(zSig,631);
  zExp = bExp;
  pushDbg(zExp,633);
  zSign ^= 1;
  goto normalizeRoundAndPack;
  aExpBigger:
  if (aExp == 0x7FF) {
    if (aSig) 
      return propagateFloat64NaN(a,b);
    return a;
  }
  if (bExp == 0) {
    --expDiff;
    pushDbg(expDiff,1030);
  }
   else 
    bSig |= 0x4000000000000000LL;
  shift64RightJamming(bSig,expDiff,&bSig);
  aSig |= 0x4000000000000000LL;
  aBigger:
  zSig = aSig - bSig;
  pushDbgLong(zSig,639);
  zExp = aExp;
  pushDbg(zExp,641);
  normalizeRoundAndPack:
  --zExp;
  pushDbg(zExp,1034);
  return normalizeRoundAndPackFloat64(zSign,zExp,zSig);
}
/*----------------------------------------------------------------------------
| Returns the result of adding the double-precision floating-point values `a'
| and `b'.  The operation is performed according to the IEC/IEEE Standard for
| Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

float64 float64_add(float64 a,float64 b)
{
  flag aSign;
  flag bSign;
  aSign = extractFloat64Sign(a);
  pushDbg(aSign,642);
  bSign = extractFloat64Sign(b);
  pushDbg(bSign,643);
  if (aSign == bSign) 
    return addFloat64Sigs(a,b,aSign);
   else 
    return subFloat64Sigs(a,b,aSign);
}
/*----------------------------------------------------------------------------
| Returns the result of multiplying the double-precision floating-point values
| `a' and `b'.  The operation is performed according to the IEC/IEEE Standard
| for Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

float64 float64_mul(float64 a,float64 b)
{
  flag aSign;
  flag bSign;
  flag zSign;
  int16 aExp;
  int16 bExp;
  int16 zExp;
  bits64 aSig;
  bits64 bSig;
  bits64 zSig0;
  bits64 zSig1;
  aSig = extractFloat64Frac(a);
  pushDbgLong(aSig,645);
  aExp = extractFloat64Exp(a);
  pushDbg(aExp,646);
  aSign = extractFloat64Sign(a);
  pushDbg(aSign,647);
  bSig = extractFloat64Frac(b);
  pushDbgLong(bSig,648);
  bExp = extractFloat64Exp(b);
  pushDbg(bExp,649);
  bSign = extractFloat64Sign(b);
  pushDbg(bSign,650);
  zSign = aSign ^ bSign;
  pushDbg(zSign,651);
  if (aExp == 0x7FF) {
    if (aSig || bExp == 0x7FF && bSig) 
      return propagateFloat64NaN(a,b);
    if ((bExp | bSig) == 0) {
      float_raise(16);
      return 0x7FFFFFFFFFFFFFFFLL;
    }
    return packFloat64(zSign,0x7FF,0);
  }
  if (bExp == 0x7FF) {
    if (bSig) 
      return propagateFloat64NaN(a,b);
    if ((aExp | aSig) == 0) {
      float_raise(16);
      return 0x7FFFFFFFFFFFFFFFLL;
    }
    return packFloat64(zSign,0x7FF,0);
  }
  if (aExp == 0) {
    if (aSig == 0) 
      return packFloat64(zSign,0,0);
    normalizeFloat64Subnormal(aSig,&aExp,&aSig);
  }
  if (bExp == 0) {
    if (bSig == 0) 
      return packFloat64(zSign,0,0);
    normalizeFloat64Subnormal(bSig,&bExp,&bSig);
  }
  zExp = aExp + bExp - 0x3FF;
  pushDbg(zExp,666);
  aSig = (aSig | 0x0010000000000000LL) << 10;
  pushDbgLong(aSig,669);
  bSig = (bSig | 0x0010000000000000LL) << 11;
  pushDbgLong(bSig,672);
  mul64To128(aSig,bSig,&zSig0,&zSig1);
  zSig0 |= (zSig1 != 0);
  if (0 <= ((sbits64 )(zSig0 << 1))) {
    zSig0 <<= 1;
    --zExp;
    pushDbg(zExp,1059);
  }
  return roundAndPackFloat64(zSign,zExp,zSig0);
}
/*----------------------------------------------------------------------------
| Returns the result of dividing the double-precision floating-point value `a'
| by the corresponding value `b'.  The operation is performed according to
| the IEC/IEEE Standard for Binary Floating-Point Arithmetic.
*----------------------------------------------------------------------------*/

float64 float64_div(float64 a,float64 b)
{
  flag aSign;
  flag bSign;
  flag zSign;
  int16 aExp;
  int16 bExp;
  int16 zExp;
  bits64 aSig;
  bits64 bSig;
  bits64 zSig;
  bits64 rem0;
  bits64 rem1;
  bits64 term0;
  bits64 term1;
  aSig = extractFloat64Frac(a);
  pushDbgLong(aSig,680);
  aExp = extractFloat64Exp(a);
  pushDbg(aExp,681);
  aSign = extractFloat64Sign(a);
  pushDbg(aSign,682);
  bSig = extractFloat64Frac(b);
  pushDbgLong(bSig,683);
  bExp = extractFloat64Exp(b);
  pushDbg(bExp,684);
  bSign = extractFloat64Sign(b);
  pushDbg(bSign,685);
  zSign = aSign ^ bSign;
  pushDbg(zSign,686);
  if (aExp == 0x7FF) {
    if (aSig) 
      return propagateFloat64NaN(a,b);
    if (bExp == 0x7FF) {
      if (bSig) 
        return propagateFloat64NaN(a,b);
      float_raise(16);
      return 0x7FFFFFFFFFFFFFFFLL;
    }
    return packFloat64(zSign,0x7FF,0);
  }
  if (bExp == 0x7FF) {
    if (bSig) 
      return propagateFloat64NaN(a,b);
    return packFloat64(zSign,0,0);
  }
  if (bExp == 0) {
    if (bSig == 0) {
      if ((aExp | aSig) == 0) {
        float_raise(16);
        return 0x7FFFFFFFFFFFFFFFLL;
      }
      float_raise(2);
      return packFloat64(zSign,0x7FF,0);
    }
    normalizeFloat64Subnormal(bSig,&bExp,&bSig);
  }
  if (aExp == 0) {
    if (aSig == 0) 
      return packFloat64(zSign,0,0);
    normalizeFloat64Subnormal(aSig,&aExp,&aSig);
  }
  zExp = aExp - bExp + 0x3FD;
  pushDbg(zExp,697);
  aSig = (aSig | 0x0010000000000000LL) << 10;
  pushDbgLong(aSig,700);
  bSig = (bSig | 0x0010000000000000LL) << 11;
  pushDbgLong(bSig,703);
  if (bSig <= aSig + aSig) {
    aSig >>= 1;
    ++zExp;
    pushDbg(zExp,1076);
  }
  zSig = estimateDiv128To64(aSig,0,bSig);
  pushDbgLong(zSig,709);
  if ((zSig & 0x1FF) <= 2) {
    mul64To128(bSig,zSig,&term0,&term1);
    sub128(aSig,0,term0,term1,&rem0,&rem1);
    while(((sbits64 )rem0) < 0){
      --zSig;
      pushDbgLong(zSig,1087);
      add128(rem0,rem1,0,bSig,&rem0,&rem1);
    }
    zSig |= (rem1 != 0);
  }
  return roundAndPackFloat64(zSign,zExp,zSig);
}
/*----------------------------------------------------------------------------
| Returns 1 if the double-precision floating-point value `a' is less than or
| equal to the corresponding value `b', and 0 otherwise.  The comparison is
| performed according to the IEC/IEEE Standard for Binary Floating-Point
| Arithmetic.
*----------------------------------------------------------------------------*/

flag float64_le(float64 a,float64 b)
{
  flag aSign;
  flag bSign;
  if (extractFloat64Exp(a) == 0x7FF && extractFloat64Frac(a) || extractFloat64Exp(b) == 0x7FF && extractFloat64Frac(b)) {
    float_raise(16);
    return 0;
  }
  aSign = extractFloat64Sign(a);
  pushDbg(aSign,720);
  bSign = extractFloat64Sign(b);
  pushDbg(bSign,721);
  if (aSign != bSign) 
    return aSign || ((bits64 )((a | b) << 1)) == 0;
  return a == b || aSign ^ a < b;
}

flag float64_ge(float64 a,float64 b)
{
  return float64_le(b,a);
}
// added by hiroyuki@acm.org

float64 float64_neg(float64 x)
{
  return ~x & 0x8000000000000000LL | x & 0x7fffffffffffffffULL;
}
