volatile unsigned long TRACEBUFFER[256];
unsigned char bufIndex;
unsigned long traceOut;

void pushDbg(int data,int ID)
{
  TRACEBUFFER[bufIndex++] = ((unsigned long )ID) << 32 | ((unsigned int )data);
}

void pushDbgLong(long dataL,int ID)
{
  TRACEBUFFER[bufIndex++] = ((unsigned long )ID) << 32 | ((unsigned int )(dataL >> 32));
  TRACEBUFFER[bufIndex++] = ((unsigned long )ID) << 32 | ((unsigned int )dataL);
}
void traceUnload();
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/* NIST Secure Hash Algorithm */
/* heavily modified by Uwe Hollerbach uh@alumni.caltech edu */
/* from Peter C. Gutmann's implementation as found in */
/* Applied Cryptography by Bruce Schneier */
/* NIST's proposed modification to SHA of 7/11/94 may be */
/* activated by defining USE_MODIFIED_SHA */
#include <stdio.h>
#include "sha.h"
#include "sha.c"
/*
+--------------------------------------------------------------------------+
| * Test Vector (added for CHStone)                                        |
|     outData : expected output data                                       |
+--------------------------------------------------------------------------+
*/
const LONG outData[5] = {(0x006a5a37UL), (0x93dc9485UL), (0x2c412112UL), (0x63f7ba43UL), (0xad73f922UL)};

int sha_main()
{
  
#pragma HLS interface port=traceOut
  int i;
  int main_result;
  main_result = 0;
  sha_stream();
  for (i = 0; i < 5; i++) {
    main_result += sha_info_digest[i] == outData[i];
  }
  printf("Result: %d\n",main_result);
  if (main_result == 5) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  traceUnload();
  return main_result;
}

void traceUnload()
{
  int i;
  for (i = 0; i < 256; i++) 
    traceOut = TRACEBUFFER[i];
  for (i = 0; i < 2; i++) 
    traceOut = __val[i];
  for (i = 0; i < 4; i++) 
    traceOut = __wchb[i];
  for (i = 0; i < 1; i++) 
    traceOut = _shortbuf[i];
  for (i = 0; i < 5; i++) 
    traceOut = sha_info_digest[i];
  for (i = 0; i < 16; i++) 
    traceOut = sha_info_data[i];
  for (i = 0; i < 80; i++) 
    traceOut = W[i];
  for (i = 0; i < 5; i++) 
    traceOut = sha_info_digest[i];
  for (i = 0; i < 16; i++) 
    traceOut = sha_info_data[i];
  for (i = 0; i < 80; i++) 
    traceOut = W[i];
}
