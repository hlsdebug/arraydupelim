/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/* NIST Secure Hash Algorithm */
/* heavily modified by Uwe Hollerbach uh@alumni.caltech edu */
/* from Peter C. Gutmann's implementation as found in */
/* Applied Cryptography by Bruce Schneier */
/* NIST's proposed modification to SHA of 7/11/94 may be */
/* activated by defining USE_MODIFIED_SHA */
#include "sha.h"
/* SHA f()-functions */
#define f1(x,y,z)	((x & y) | (~x & z))
#define f2(x,y,z)	(x ^ y ^ z)
#define f3(x,y,z)	((x & y) | (x & z) | (y & z))
#define f4(x,y,z)	(x ^ y ^ z)
/* SHA constants */
#define CONST1		0x5a827999L
#define CONST2		0x6ed9eba1L
#define CONST3		0x8f1bbcdcL
#define CONST4		0xca62c1d6L
/* 32-bit rotate */
#define ROT32(x,n)	((x << n) | (x >> (32 - n)))
#define FUNC(n,i)						\
    temp = ROT32(A,5) + f##n(B,C,D) + E + W[i] + CONST##n;	\
    E = D; D = C; C = ROT32(B,30); B = A; A = temp

void sha_memset(LONG *s,int c,int n,int e)
{
  LONG uc;
  LONG *p;
  int m;
  m = n / 4;
  pushDbg(m,212);
  uc = c;
  pushDbg(uc,214);
  p = ((LONG *)s);
  while(e-- > 0){
    p++;
  }
  pushDbg(e,33256);
  while(m-- > 0){
    LONG *temp_ptr_p_54_66;
    temp_ptr_p_54_66 = p++;
    pushDbg( *temp_ptr_p_54_66,218);
     *temp_ptr_p_54_66 = uc;
  }
  pushDbg(m,33258);
}

void sha_memcpy(LONG *s1,const BYTE *s2,int n)
{
  LONG *p1;
  BYTE *p2;
  LONG tmp;
  int m;
  m = n / 4;
  pushDbg(m,219);
  p1 = ((LONG *)s1);
  p2 = ((BYTE *)s2);
  while(m-- > 0){
    tmp = 0;
    pushDbg(tmp,224);
    tmp |= (0xFF & ( *(p2++)));
    tmp |= ((0xFF & ( *(p2++))) << 8);
    tmp |= ((0xFF & ( *(p2++))) << 16);
    tmp |= ((0xFF & ( *(p2++))) << 24);
    LONG *temp_ptr_p1_73_88;
    temp_ptr_p1_73_88 = p1;
    pushDbg( *temp_ptr_p1_73_88,236);
     *temp_ptr_p1_73_88 = tmp;
    p1++;
  }
  pushDbg(m,33263);
}
/* do SHA transformation */

static void sha_transform()
{
  int i;
  LONG temp;
  LONG A;
  LONG B;
  LONG C;
  LONG D;
  LONG E;
  LONG W[80];
  for (i = 0; i < 16; ++i) {
    LONG *temp_ptr_W_99_103;
    temp_ptr_W_99_103 = &W[i];
    pushDbg( *temp_ptr_W_99_103,239);
     *temp_ptr_W_99_103 = sha_info_data[i];
  }
  for (i = 16; i < 80; ++i) {
    LONG *temp_ptr_W_99_107;
    temp_ptr_W_99_107 = &W[i];
    pushDbg( *temp_ptr_W_99_107,244);
     *temp_ptr_W_99_107 = W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16];
  }
  A = sha_info_digest[0];
  pushDbg(A,257);
  B = sha_info_digest[1];
  pushDbg(B,259);
  C = sha_info_digest[2];
  pushDbg(C,261);
  D = sha_info_digest[3];
  pushDbg(D,263);
  E = sha_info_digest[4];
  pushDbg(E,265);
  for (i = 0; i < 20; ++i) {
    temp = (((A << 5 | A >> 32 - 5) + (B & C | ~B & D) + E + W[i]) + 0x5a827999L);
    pushDbg(temp,269);
    E = D;
    pushDbg(E,282);
    D = C;
    pushDbg(D,283);
    C = B << 30 | B >> 32 - 30;
    pushDbg(C,284);
    B = A;
    pushDbg(B,289);
    A = temp;
    pushDbg(A,290);
  }
  for (i = 20; i < 40; ++i) {
    temp = (((A << 5 | A >> 32 - 5) + (B ^ C ^ D) + E + W[i]) + 0x6ed9eba1L);
    pushDbg(temp,293);
    E = D;
    pushDbg(E,305);
    D = C;
    pushDbg(D,306);
    C = B << 30 | B >> 32 - 30;
    pushDbg(C,307);
    B = A;
    pushDbg(B,312);
    A = temp;
    pushDbg(A,313);
  }
  for (i = 40; i < 60; ++i) {
    temp = (((A << 5 | A >> 32 - 5) + (B & C | B & D | C & D) + E + W[i]) + 0x8f1bbcdcL);
    pushDbg(temp,316);
    E = D;
    pushDbg(E,331);
    D = C;
    pushDbg(D,332);
    C = B << 30 | B >> 32 - 30;
    pushDbg(C,333);
    B = A;
    pushDbg(B,338);
    A = temp;
    pushDbg(A,339);
  }
  for (i = 60; i < 80; ++i) {
    temp = (((A << 5 | A >> 32 - 5) + (B ^ C ^ D) + E + W[i]) + 0xca62c1d6L);
    pushDbg(temp,342);
    E = D;
    pushDbg(E,354);
    D = C;
    pushDbg(D,355);
    C = B << 30 | B >> 32 - 30;
    pushDbg(C,356);
    B = A;
    pushDbg(B,361);
    A = temp;
    pushDbg(A,362);
  }
  sha_info_digest[0] += A;
  sha_info_digest[1] += B;
  sha_info_digest[2] += C;
  sha_info_digest[3] += D;
  sha_info_digest[4] += E;
}
/* initialize the SHA digest */

void sha_init()
{
  LONG *temp_ptr_sha_info_digest_32_144;
  temp_ptr_sha_info_digest_32_144 = &sha_info_digest[0];
  pushDbg( *temp_ptr_sha_info_digest_32_144,373);
   *temp_ptr_sha_info_digest_32_144 = 0x67452301L;
  LONG *temp_ptr_sha_info_digest_32_145;
  temp_ptr_sha_info_digest_32_145 = &sha_info_digest[1];
  pushDbg( *temp_ptr_sha_info_digest_32_145,375);
   *temp_ptr_sha_info_digest_32_145 = 0xefcdab89L;
  LONG *temp_ptr_sha_info_digest_32_146;
  temp_ptr_sha_info_digest_32_146 = &sha_info_digest[2];
  pushDbg( *temp_ptr_sha_info_digest_32_146,377);
   *temp_ptr_sha_info_digest_32_146 = 0x98badcfeL;
  LONG *temp_ptr_sha_info_digest_32_147;
  temp_ptr_sha_info_digest_32_147 = &sha_info_digest[3];
  pushDbg( *temp_ptr_sha_info_digest_32_147,379);
   *temp_ptr_sha_info_digest_32_147 = 0x10325476L;
  LONG *temp_ptr_sha_info_digest_32_148;
  temp_ptr_sha_info_digest_32_148 = &sha_info_digest[4];
  pushDbg( *temp_ptr_sha_info_digest_32_148,381);
   *temp_ptr_sha_info_digest_32_148 = 0xc3d2e1f0L;
  sha_info_count_lo = 0L;
  pushDbg(sha_info_count_lo,383);
  sha_info_count_hi = 0L;
  pushDbg(sha_info_count_hi,384);
}
/* update the SHA digest */

void sha_update(const BYTE *buffer,int count)
{
  if (sha_info_count_lo + (((LONG )count) << 3) < sha_info_count_lo) {
    ++sha_info_count_hi;
    pushDbg(sha_info_count_hi,33306);
  }
  sha_info_count_lo += ((LONG )count) << 3;
  sha_info_count_hi += ((LONG )count) >> 29;
  while(count >= 64){
    sha_memcpy(sha_info_data,buffer,64);
    sha_transform();
    buffer += 64;
    count -= 64;
  }
  sha_memcpy(sha_info_data,buffer,count);
}
/* finish computing the SHA digest */

void sha_final()
{
  int count;
  LONG lo_bit_count;
  LONG hi_bit_count;
  lo_bit_count = sha_info_count_lo;
  pushDbg(lo_bit_count,395);
  hi_bit_count = sha_info_count_hi;
  pushDbg(hi_bit_count,396);
  count = ((int )(lo_bit_count >> 3 & 0x3f));
  pushDbg(count,397);
  LONG *temp_ptr_sha_info_data_34_186;
  temp_ptr_sha_info_data_34_186 = &sha_info_data[count++];
  pushDbg(count,33311);
  pushDbg( *temp_ptr_sha_info_data_34_186,400);
   *temp_ptr_sha_info_data_34_186 = 0x80;
  if (count > 56) {
    sha_memset(sha_info_data,0,64 - count,count);
    sha_transform();
    sha_memset(sha_info_data,0,56,0);
  }
   else {
    sha_memset(sha_info_data,0,56 - count,count);
  }
  LONG *temp_ptr_sha_info_data_34_197;
  temp_ptr_sha_info_data_34_197 = &sha_info_data[14];
  pushDbg( *temp_ptr_sha_info_data_34_197,405);
   *temp_ptr_sha_info_data_34_197 = hi_bit_count;
  LONG *temp_ptr_sha_info_data_34_198;
  temp_ptr_sha_info_data_34_198 = &sha_info_data[15];
  pushDbg( *temp_ptr_sha_info_data_34_198,407);
   *temp_ptr_sha_info_data_34_198 = lo_bit_count;
  sha_transform();
}
/* compute the SHA digest of a FILE stream */

void sha_stream()
{
  int i;
  int j;
  const BYTE *p;
  sha_init();
  for (j = 0; j < 2; j++) {
    i = in_i[j];
    pushDbg(i,411);
    p = &indata[j][0];
    sha_update(p,i);
  }
  sha_final();
}
