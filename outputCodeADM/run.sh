#!/bin/bash

rm ./*/*c

cd adpcm
../../addShBufADM --top adpcm_main ../../chstone/adpcm/adpcm.c
cd ../aes
../../addShBufADM --top aes_main ../../chstone/aes/aes.c ../../chstone/aes/aes_dec.c ../../chstone/aes/aes_enc.c ../../chstone/aes/aes_func.c ../../chstone/aes/aes_key.c
cd ../blowfish
../../addShBufADM --top blowfish_main ../../chstone/blowfish/bf.c ../../chstone/blowfish/bf_cfb64.c ../../chstone/blowfish/bf_enc.c ../../chstone/blowfish/bf_skey.c
cd ../dfadd
../../addShBufADM --top dfadd_main ../../chstone/dfadd/dfadd.c ../../chstone/dfadd/softfloat.c
cd ../dfdiv
../../addShBufADM --top dfdiv_main ../../chstone/dfdiv/dfdiv.c ../../chstone/dfdiv/softfloat.c
cd ../dfmul
../../addShBufADM --top dfmul_main ../../chstone/dfmul/dfmul.c ../../chstone/dfmul/softfloat.c
cd ../dfsin
../../addShBufADM --top dfsin_main ../../chstone/dfsin/dfsin.c ../../chstone/dfsin/softfloat.c
cd ../gsm
../../addShBufADM --top gsm_main ../../chstone/gsm/gsm.c ../../chstone/gsm/add.c ../../chstone/gsm/lpc.c
cd ../jpeg
../../addShBufADM --top jpeg_main ../../chstone/jpeg/main.c ../../chstone/jpeg/chenidct.c ../../chstone/jpeg/decode.c ../../chstone/jpeg/huffman.c ../../chstone/jpeg/jpeg2bmp.c ../../chstone/jpeg/jfif_read.c ../../chstone/jpeg/marker.c
cd ../mips
../../addShBufADM --top mips_main ../../chstone/mips/mips.c
cd ../motion
../../addShBufADM --top motion_main ../../chstone/motion/mpeg2.c ../../chstone/motion/motion.c ../../chstone/motion/getbits.c ../../chstone/motion/getvlc.c
cd ../sha
../../addShBufADM --top sha_main ../../chstone/sha/sha_driver.c ../../chstone/sha/sha.c

cd ..

cp -R ./ /home/legup/Xilinx/workspace/CHSTONE_ARR/
rm /home/legup/Xilinx/workspace/CHSTONE_ARR/run.sh

rename 's/rose_//g' ./*/*.c
