/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 *  Read the head of the marker
 *
 *  @(#) $Id: marker.c,v 1.2 2003/07/18 10:19:21 honda Exp $
 */
/*************************************************************
Copyright (C) 1990, 1991, 1993 Andy C. Hung, all rights reserved.
PUBLIC DOMAIN LICENSE: Stanford University Portable Video Research
Group. If you use this software, you agree to the following: This
program package is purely experimental, and is licensed "as is".
Permission is granted to use, modify, and distribute this program
without charge for any purpose, provided this license/ disclaimer
notice appears in the copies.  No warranty or maintenance is given,
either expressed or implied.  In no event shall the author(s) be
liable to you or a third party for any special, incidental,
consequential, or other damages, arising out of the use or inability
to use the program for any purpose (or the loss of data), even if we
have been advised of such possibilities.  Any public reference or
advertisement of this source code should refer to it as the Portable
Video Research Group (PVRG) code, and not by any author(s) (or
Stanford University) name.
*************************************************************/
/*
************************************************************
marker.c
This file contains the Marker library which uses the direct buffer
access routines bgetc...
************************************************************
*/
#include "decode.h"
#include "huffman.h"
#include "global.h"
#include "init.h"
/* Only for the marker needed at the baseline */
/* JPEG marker codes */
#define    M_SOI   0xd8  /* Start of Image */
#define    M_SOF0  0xc0  /* Baseline DCT ( Huffman ) */
#define    M_SOS   0xda  /* Start of Scan ( Head of Compressed Data ) */
#define    M_DHT   0xc4  /* Huffman Table */
#define    M_DQT   0xdb  /* Quantization Table */
#define    M_EOI   0xd9  /* End of Image */
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone) for "read_markers"                    |
|     out_unread_marker : expected output data                             |
+--------------------------------------------------------------------------+
*/
#define READ_MARKERS_REP 10
int i_marker = 0;
const int out_unread_marker[10] = {(0xd8), (0xe0), (0xdb), (0xdb), (0xc0), (0xc4), (0xc4), (0xc4), (0xc4), (0xda)};
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone) for "get_sof"                         |
|     out_length_get_sof : expected output data                            |
|     out_data_precision_get_sof : expected output data                    |
|     out_p_jinfo_image_height_get_sof : expected output data              |
|     out_p_jinfo_image_width_get_sof : expected output data               |
|     out_p_jinfo_num_components_get_sof : expected output data            |
|     out_index_get_sof : expected output data                             |
|     out_id_get_sof : expected output data                                |
|     out_h_samp_factor_get_sof : expected output data                     |
|     out_v_samp_factor_get_sof : expected output data                     |
|     out_quant_tbl_no_get_sof : expected output data                      |
+--------------------------------------------------------------------------+
*/
#define GET_SOF_REP 3
int out_length_get_sof = 17;
int out_data_precision_get_sof = 8;
int out_p_jinfo_image_height_get_sof = 59;
int out_p_jinfo_image_width_get_sof = 90;
int out_p_jinfo_num_components_get_sof = 3;
const int out_index_get_sof[3] = {(0), (1), (2)};
const int out_id_get_sof[3] = {(1), (2), (3)};
const int out_h_samp_factor_get_sof[3] = {(2), (1), (1)};
const int out_v_samp_factor_get_sof[3] = {(2), (1), (1)};
const int out_quant_tbl_no_get_sof[3] = {(0), (1), (1)};
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone) for "get_sos"                         |
|     out_length_get_sos : expected output data                            |
|     out_num_comp_get_sos : expected output data                          |
|     out_comp_id_get_sos : expected output data                           |
|     out_dc_tbl_no_get_sos : expected output data                         |
|     out_ac_tbl_no_get_sos : expected output data                         |
+--------------------------------------------------------------------------+
*/
#define GET_SOS_REP 3
int i_get_sos = 0;
int out_length_get_sos = 12;
int out_num_comp_get_sos = 3;
const int out_comp_id_get_sos[3] = {(1), (2), (3)};
const int out_dc_tbl_no_get_sos[3] = {(0), (1), (1)};
const int out_ac_tbl_no_get_sos[3] = {(0), (1), (1)};
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone) for "get_dht"                         |
|     out_length_get_dht : expected output data                            |
|     out_index_get_dht : expected output data                             |
|     out_count_get_dht : expected output data                             |
+--------------------------------------------------------------------------+
*/
#define GET_DHT_REP 4
int i_get_dht = 0;
const int out_length_get_dht[4] = {(29), (179), (29), (179)};
const int out_index_get_dht[4] = {(0x0), (0x10), (0x1), (0x11)};
const int out_count_get_dht[4] = {(12), (162), (12), (162)};
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone) for "get_dqt"                         |
|     out_length_get_dqt : expected output data                            |
|     out_prec_get_dht : expected output data                              |
|     out_num_get_dht : expected output data                               |
+--------------------------------------------------------------------------+
*/
#define GET_DQT_REP 2
int i_get_dqt = 0;
const int out_length_get_dqt[2] = {(65), (65)};
const int out_prec_get_dht[2] = {(0), (0)};
const int out_num_get_dht[2] = {(0), (1)};
/*
 * Initialize Quantization Table
 */
const int izigzag_index[64] = {(0), (1), (8), (16), (9), (2), (3), (10), (17), (24), (32), (25), (18), (11), (4), (5), (12), (19), (26), (33), (40), (48), (41), (34), (27), (20), (13), (6), (7), (14), (21), (28), (35), (42), (49), (56), (57), (50), (43), (36), (29), (22), (15), (23), (30), (37), (44), (51), (58), (59), (52), (45), (38), (31), (39), (46), (53), (60), (61), (54), (47), (55), (62), (63)};
/*
 *  Read Buffer
 */
static unsigned char *ReadBuf;
/*
 *  Read from Buffer
 */

int read_byte()
{
  return ( *(ReadBuf++));
}

short read_word()
{
  short c;
  c = (( *(ReadBuf++)) << 8);
  pushDbg(c,1430);
  c |= ( *(ReadBuf++));
  return c;
}

int read_dword()
{
  int c;
  c = ( *(ReadBuf++)) << 24;
  pushDbg(c,1433);
  c |= ( *(ReadBuf++)) << 16;
  c |= ( *(ReadBuf++)) << 8;
  c |= ( *(ReadBuf++));
  return c;
}

int first_marker()
{
  int c1;
  int c2;
  c1 = read_byte();
  pushDbg(c1,1440);
  c2 = read_byte();
  pushDbg(c2,1441);
  if (c1 != 0xFF || c2 != 0xd8) {
    printf("Not Jpeg File!\n");
    exit(0);
  }
   else {
    main_result++;
    pushDbg(main_result,129205);
  }
  return c2;
}

int next_marker()
{
  int c;
  for (; ; ) {
    c = read_byte();
    pushDbg(c,1445);
    while(c != 0xFF){
      c = read_byte();
      pushDbg(c,1447);
    }
    do {
      c = read_byte();
      pushDbg(c,1448);
    }while (c == 0xFF);
    if (c != 0) 
      break; 
  }
  return c;
}
/*
 *  Baseline DCT ( Huffman )  
 */

void get_sof()
{
  int ci;
  int c;
  int length;
  char *p_comp_info_index;
  char *p_comp_info_id;
  char *p_comp_info_h_samp_factor;
  char *p_comp_info_v_samp_factor;
  char *p_comp_info_quant_tbl_no;
  char *p_comp_info_dc_tbl_no;
  char *p_comp_info_ac_tbl_no;
  length = (read_word());
  pushDbg(length,1451);
  p_jinfo_data_precision = (read_byte());
  pushDbg(p_jinfo_data_precision,1452);
  p_jinfo_image_height = read_word();
  pushDbg(p_jinfo_image_height,1453);
  p_jinfo_image_width = read_word();
  pushDbg(p_jinfo_image_width,1454);
  p_jinfo_num_components = (read_byte());
  pushDbg(p_jinfo_num_components,1455);
  printf("length         = %d\n",length);
  printf("data_precision = %d\n",p_jinfo_data_precision);
  printf("image_height   = %d\n",p_jinfo_image_height);
  printf("image_width    = %d\n",p_jinfo_image_width);
  printf("num_components = %d\n",p_jinfo_num_components);
  if (length == out_length_get_sof) {
    main_result++;
    pushDbg(main_result,129213);
  }
  if (p_jinfo_data_precision == out_data_precision_get_sof) {
    main_result++;
    pushDbg(main_result,129215);
  }
  if (p_jinfo_image_height == out_p_jinfo_image_height_get_sof) {
    main_result++;
    pushDbg(main_result,129217);
  }
  if (p_jinfo_image_width == out_p_jinfo_image_width_get_sof) {
    main_result++;
    pushDbg(main_result,129219);
  }
  if (p_jinfo_num_components == out_p_jinfo_num_components_get_sof) {
    main_result++;
    pushDbg(main_result,129221);
  }
  length -= 8;
/* Omit error check */
/* Check components */
  for (ci = 0; ci < p_jinfo_num_components; ci++) {
    p_comp_info_index = &p_jinfo_comps_info_index[ci];
    p_comp_info_id = &p_jinfo_comps_info_id[ci];
    p_comp_info_h_samp_factor = &p_jinfo_comps_info_h_samp_factor[ci];
    p_comp_info_v_samp_factor = &p_jinfo_comps_info_v_samp_factor[ci];
    p_comp_info_quant_tbl_no = &p_jinfo_comps_info_quant_tbl_no[ci];
    char *temp_ptr_p_comp_info_index_252_306;
    temp_ptr_p_comp_info_index_252_306 = p_comp_info_index;
    pushDbg( *temp_ptr_p_comp_info_index_252_306,1474);
     *temp_ptr_p_comp_info_index_252_306 = ci;
    char *temp_ptr_p_comp_info_id_253_307;
    temp_ptr_p_comp_info_id_253_307 = p_comp_info_id;
    pushDbg( *temp_ptr_p_comp_info_id_253_307,1475);
     *temp_ptr_p_comp_info_id_253_307 = (read_byte());
    c = read_byte();
    pushDbg(c,1476);
    char *temp_ptr_p_comp_info_h_samp_factor_254_309;
    temp_ptr_p_comp_info_h_samp_factor_254_309 = p_comp_info_h_samp_factor;
    pushDbg( *temp_ptr_p_comp_info_h_samp_factor_254_309,1477);
     *temp_ptr_p_comp_info_h_samp_factor_254_309 = (c >> 4 & 15);
    char *temp_ptr_p_comp_info_v_samp_factor_255_310;
    temp_ptr_p_comp_info_v_samp_factor_255_310 = p_comp_info_v_samp_factor;
    pushDbg( *temp_ptr_p_comp_info_v_samp_factor_255_310,1480);
     *temp_ptr_p_comp_info_v_samp_factor_255_310 = (c & 15);
    char *temp_ptr_p_comp_info_quant_tbl_no_256_311;
    temp_ptr_p_comp_info_quant_tbl_no_256_311 = p_comp_info_quant_tbl_no;
    pushDbg( *temp_ptr_p_comp_info_quant_tbl_no_256_311,1482);
     *temp_ptr_p_comp_info_quant_tbl_no_256_311 = (read_byte());
    printf(" index         = %d\n",( *p_comp_info_index));
    printf(" id            = %d\n",( *p_comp_info_id));
    printf(" h_samp_factor = %d\n",( *p_comp_info_h_samp_factor));
    printf(" v_samp_factor = %d\n",( *p_comp_info_v_samp_factor));
    printf(" quant_tbl_no  = %d\n\n",( *p_comp_info_quant_tbl_no));
    if (( *p_comp_info_index) == out_index_get_sof[ci]) {
      main_result++;
      pushDbg(main_result,129251);
    }
    if (( *p_comp_info_id) == out_id_get_sof[ci]) {
      main_result++;
      pushDbg(main_result,129254);
    }
    if (( *p_comp_info_h_samp_factor) == out_h_samp_factor_get_sof[ci]) {
      main_result++;
      pushDbg(main_result,129257);
    }
    if (( *p_comp_info_v_samp_factor) == out_v_samp_factor_get_sof[ci]) {
      main_result++;
      pushDbg(main_result,129260);
    }
    if (( *p_comp_info_quant_tbl_no) == out_quant_tbl_no_get_sof[ci]) {
      main_result++;
      pushDbg(main_result,129263);
    }
  }
/*
     *  Determine Sampling Factor
     *  Only 1_1_1 and 4_1_1 are supported
     */
  if (p_jinfo_comps_info_h_samp_factor[0] == 2) {
    p_jinfo_smp_fact = 2;
    printf("\nSampling Factor is 4:1:1\n");
  }
   else {
    p_jinfo_smp_fact = 0;
    printf("\nSampling Factor is 1:1:1\n");
  }
}

void get_sos()
{
  int length;
  int num_comp;
  int i;
  int c;
  int cc;
  int ci;
  int j;
  char *p_comp_info_index;
  char *p_comp_info_id;
  char *p_comp_info_h_samp_factor;
  char *p_comp_info_v_samp_factor;
  char *p_comp_info_quant_tbl_no;
  char *p_comp_info_dc_tbl_no;
  char *p_comp_info_ac_tbl_no;
  length = (read_word());
  pushDbg(length,1497);
  num_comp = read_byte();
  pushDbg(num_comp,1498);
  printf(" length = %d\n",length);
  printf(" num_comp = %d\n",num_comp);
  if (length == out_length_get_sos) {
    main_result++;
    pushDbg(main_result,129266);
  }
  if (num_comp == out_num_comp_get_sos) {
    main_result++;
    pushDbg(main_result,129267);
  }
/* Decode each component */
  for (i = 0; i < num_comp; i++) {
    cc = read_byte();
    pushDbg(cc,1503);
    c = read_byte();
    pushDbg(c,1504);
    for (ci = 0; ci < p_jinfo_num_components; ci++) {
      p_comp_info_id = &p_jinfo_comps_info_id[ci];
      p_comp_info_dc_tbl_no = &p_jinfo_comps_info_dc_tbl_no[ci];
      p_comp_info_ac_tbl_no = &p_jinfo_comps_info_ac_tbl_no[ci];
      if (cc == ( *p_comp_info_id)) 
        goto id_found;
    }
    printf("Bad Component ID!\n");
    exit(0);
    id_found:
    main_result++;
    pushDbg(main_result,129276);
    char *temp_ptr_p_comp_info_dc_tbl_no_366_402;
    temp_ptr_p_comp_info_dc_tbl_no_366_402 = p_comp_info_dc_tbl_no;
    pushDbg( *temp_ptr_p_comp_info_dc_tbl_no_366_402,1514);
     *temp_ptr_p_comp_info_dc_tbl_no_366_402 = (c >> 4 & 15);
    char *temp_ptr_p_comp_info_ac_tbl_no_367_403;
    temp_ptr_p_comp_info_ac_tbl_no_367_403 = p_comp_info_ac_tbl_no;
    pushDbg( *temp_ptr_p_comp_info_ac_tbl_no_367_403,1517);
     *temp_ptr_p_comp_info_ac_tbl_no_367_403 = (c & 15);
    printf(" comp_id       = %d\n",cc);
    printf(" dc_tbl_no     = %d\n",( *p_comp_info_dc_tbl_no));
    printf(" ac_tbl_no     = %d\n",( *p_comp_info_ac_tbl_no));
    if (cc == out_comp_id_get_sos[i_get_sos]) {
      main_result++;
      pushDbg(main_result,129285);
    }
    if (( *p_comp_info_dc_tbl_no) == out_dc_tbl_no_get_sos[i_get_sos]) {
      main_result++;
      pushDbg(main_result,129288);
    }
    if (( *p_comp_info_ac_tbl_no) == out_ac_tbl_no_get_sos[i_get_sos]) {
      main_result++;
      pushDbg(main_result,129291);
    }
    i_get_sos++;
    pushDbg(i_get_sos,129292);
  }
/* Pass parameters; Ss, Se, Ah and Al for progressive JPEG */
  j = 3;
  while(j--){
    c = read_byte();
    pushDbg(c,1526);
  }
  pushDbg(j,129293);
/*
     * Define the Buffer at this point as the head of data
     */
  p_jinfo_jpeg_data = ReadBuf;
}
/*
 * Get Huffman Table
 */

void get_dht()
{
  int length;
  int index;
  int i;
  int count;
  int *p_xhtbl_bits;
  int *p_xhtbl_huffval;
  length = (read_word());
  pushDbg(length,1528);
  length -= 2;
  printf(" length = %d\n",length);
  if (length == out_length_get_dht[i_get_dht]) {
    main_result++;
    pushDbg(main_result,129295);
  }
  while(length > 16){
    index = read_byte();
    pushDbg(index,1533);
    printf(" index = 0x%x\n",index);
    if (index == out_index_get_dht[i_get_dht]) {
      main_result++;
      pushDbg(main_result,129296);
    }
    if (index & 0x10) {
/* AC */
      index -= 0x10;
      p_xhtbl_bits = p_jinfo_ac_xhuff_tbl_bits[index];
      p_xhtbl_huffval = p_jinfo_ac_xhuff_tbl_huffval[index];
    }
     else {
/* DC */
      p_xhtbl_bits = p_jinfo_dc_xhuff_tbl_bits[index];
      p_xhtbl_huffval = p_jinfo_dc_xhuff_tbl_huffval[index];
    }
    count = 0;
    for (i = 1; i <= 16; i++) {
      int *temp_ptr_p_xhtbl_bits_448_485;
      temp_ptr_p_xhtbl_bits_448_485 = &p_xhtbl_bits[i];
      pushDbg( *temp_ptr_p_xhtbl_bits_448_485,1549);
       *temp_ptr_p_xhtbl_bits_448_485 = read_byte();
      count += p_xhtbl_bits[i];
    }
    printf(" count = %d\n",count);
    if (count == out_count_get_dht[i_get_dht]) {
      main_result++;
      pushDbg(main_result,129298);
    }
    i_get_dht++;
    pushDbg(i_get_dht,129299);
    length -= 1 + 16;
    for (i = 0; i < count; i++) {
      int *temp_ptr_p_xhtbl_huffval_449_500;
      temp_ptr_p_xhtbl_huffval_449_500 = &p_xhtbl_huffval[i];
      pushDbg( *temp_ptr_p_xhtbl_huffval_449_500,1559);
       *temp_ptr_p_xhtbl_huffval_449_500 = read_byte();
    }
    length -= count;
  }
}

void get_dqt()
{
  int length;
  int prec;
  int num;
  int i;
  unsigned int tmp;
  unsigned int *p_quant_tbl;
  length = (read_word());
  pushDbg(length,1562);
  length -= 2;
  printf(" length = %d\n",length);
  if (length == out_length_get_dqt[i_get_dqt]) {
    main_result++;
    pushDbg(main_result,129302);
  }
  while(length > 0){
    num = read_byte();
    pushDbg(num,1567);
/* Precision 0:8bit, 1:16bit*/
    prec = num >> 4;
    pushDbg(prec,1568);
/* Table Number */
    num &= 0x0f;
    printf(" prec = %d\n",prec);
    printf(" num  = %d\n",num);
    if (prec == out_prec_get_dht[i_get_dqt]) {
      main_result++;
      pushDbg(main_result,129303);
    }
    if (num == out_num_get_dht[i_get_dqt]) {
      main_result++;
      pushDbg(main_result,129304);
    }
    i_get_dqt++;
    pushDbg(i_get_dqt,129305);
    p_quant_tbl = &p_jinfo_quant_tbl_quantval[num][64];
    for (i = 0; i < 64; i++) {
      if (prec) {
        tmp = (read_word());
        pushDbg(tmp,1580);
      }
       else {
        tmp = (read_byte());
        pushDbg(tmp,1581);
      }
      unsigned int *temp_ptr_p_quant_tbl_514_552;
      temp_ptr_p_quant_tbl_514_552 = &p_quant_tbl[izigzag_index[i]];
      pushDbg( *temp_ptr_p_quant_tbl_514_552,1582);
       *temp_ptr_p_quant_tbl_514_552 = ((unsigned short )tmp);
    }
    length -= 64 + 1;
    if (prec) 
      length -= 64;
  }
}

void read_markers(unsigned char *buf)
{
  int unread_marker;
  int sow_SOI;
  ReadBuf = buf;
  sow_SOI = 0;
  unread_marker = 0;
/* Read the head of the marker */
  for (; ; ) {
    if (!sow_SOI) {
      unread_marker = first_marker();
      pushDbg(unread_marker,1591);
    }
     else {
      unread_marker = next_marker();
      pushDbg(unread_marker,1592);
    }
    printf("\nmarker = 0x%x\n",unread_marker);
    if (unread_marker == out_unread_marker[i_marker++]) {
      main_result++;
      pushDbg(main_result,129314);
    }
    pushDbg(i_marker,129313);
    switch(unread_marker){
/* Start of Image */
      case 0xd8:
{
        sow_SOI = 1;
        break; 
      }
/* Baseline DCT ( Huffman ) */
      case 0xc0:
{
        get_sof();
        break; 
      }
/* Start of Scan ( Head of Compressed Data ) */
      case 0xda:
{
        get_sos();
        return ;
      }
      case 0xc4:
{
        get_dht();
        break; 
      }
      case 0xdb:
{
        get_dqt();
        break; 
      }
      case 0xd9:
      return ;
    }
  }
}
