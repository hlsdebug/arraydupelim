/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /home/kbs/jutta/src/gsm/gsm-1.0/src/RCS/lpc.c,v 1.5 1994/12/30 23:14:54 jutta Exp $ */
#include "private.h"
#include "add.c"
/*
 *  4.2.4 .. 4.2.7 LPC ANALYSIS SECTION
 */
/* 4.2.4 */

void Autocorrelation(
/* [0..159]     IN/OUT  */
word *s,
/* [0..8]       OUT     */
longword *L_ACF)
/*
 *  The goal is to compute the array L_ACF[k].  The signal s[i] must
 *  be scaled in order to avoid an overflow situation.
 */
{
  register int k;
  register int i;
  word temp;
  word smax;
  word scalauto;
  word n;
  word *sp;
  word sl;
/*  Search for the maximum.
   */
  smax = 0;
  pushDbg(smax,568);
  for (k = 0; k <= 159; k++) {
    temp = gsm_abs(s[k]);
    pushDbg(temp,571);
    if (temp > smax) {
      smax = temp;
      pushDbg(smax,574);
    }
  }
/*  Computation of the scaling factor.
   */
  if (smax == 0) {
    scalauto = 0;
    pushDbg(scalauto,576);
  }
   else {
/* sub(4,..) */
    scalauto = (4 - (gsm_norm(((longword )smax) << 16)));
    pushDbg(scalauto,577);
  }
  if (scalauto > 0 && scalauto <= 4) {
    n = scalauto;
    pushDbg(n,583);
    for (k = 0; k <= 159; k++) {
      word *temp_ptr_s_39_75;
      temp_ptr_s_39_75 = &s[k];
      pushDbg( *temp_ptr_s_39_75,586);
       *temp_ptr_s_39_75 = gsm_mult_r(s[k],(16384 >> n - 1));
    }
  }
/*  Compute the L_ACF[..].
   */
{
    sp = s;
    sl =  *sp;
    pushDbg(sl,592);
#define STEP(k)	 L_ACF[k] += ((longword)sl * sp[ -(k) ]);
#define NEXTI	 sl = *++sp
    for (k = 8; k >= 0; k--) {
      longword *temp_ptr_L_ACF_40_88;
      temp_ptr_L_ACF_40_88 = &L_ACF[k];
      pushDbgLong( *temp_ptr_L_ACF_40_88,595);
       *temp_ptr_L_ACF_40_88 = 0;
    }
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    sl =  *(++sp);
    pushDbg(sl,601);
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    sl =  *(++sp);
    pushDbg(sl,610);
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    sl =  *(++sp);
    pushDbg(sl,623);
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    sl =  *(++sp);
    pushDbg(sl,640);
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    L_ACF[4] += ((longword )sl) * sp[- 4];
    ;
    sl =  *(++sp);
    pushDbg(sl,661);
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    L_ACF[4] += ((longword )sl) * sp[- 4];
    ;
    L_ACF[5] += ((longword )sl) * sp[- 5];
    ;
    sl =  *(++sp);
    pushDbg(sl,686);
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    L_ACF[4] += ((longword )sl) * sp[- 4];
    ;
    L_ACF[5] += ((longword )sl) * sp[- 5];
    ;
    L_ACF[6] += ((longword )sl) * sp[- 6];
    ;
    sl =  *(++sp);
    pushDbg(sl,715);
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    L_ACF[4] += ((longword )sl) * sp[- 4];
    ;
    L_ACF[5] += ((longword )sl) * sp[- 5];
    ;
    L_ACF[6] += ((longword )sl) * sp[- 6];
    ;
    L_ACF[7] += ((longword )sl) * sp[- 7];
    ;
    for (i = 8; i <= 159; i++) {
      sl =  *(++sp);
      pushDbg(sl,750);
      L_ACF[0] += ((longword )sl) * sp[- 0];
      ;
      L_ACF[1] += ((longword )sl) * sp[- 1];
      ;
      L_ACF[2] += ((longword )sl) * sp[- 2];
      ;
      L_ACF[3] += ((longword )sl) * sp[- 3];
      ;
      L_ACF[4] += ((longword )sl) * sp[- 4];
      ;
      L_ACF[5] += ((longword )sl) * sp[- 5];
      ;
      L_ACF[6] += ((longword )sl) * sp[- 6];
      ;
      L_ACF[7] += ((longword )sl) * sp[- 7];
      ;
      L_ACF[8] += ((longword )sl) * sp[- 8];
      ;
    }
    for (k = 8; k >= 0; k--) 
      L_ACF[k] <<= 1;
  }
/*   Rescaling of the array s[0..159]
   */
  if (scalauto > 0) 
    for (k = 159; k >= 0; k--) 
       *(s++) <<= scalauto;
}
/* 4.2.5 */

void Reflection_coefficients(
/* 0...8        IN      */
longword *L_ACF,
/* 0...7        OUT     */
word *r)
{
  register int i;
  register int m;
  register int n;
  register word temp;
/* 0..8 */
  word ACF[9];
/* 0..8 */
  word P[9];
/* 2..8 */
  word K[9];
/*  Schur recursion with 16 bits arithmetic.
   */
  if (L_ACF[0] == 0) {
    for (i = 8; i > 0; i--) {
      word *temp_ptr_r_165_179;
      temp_ptr_r_165_179 = r++;
      pushDbg( *temp_ptr_r_165_179,799);
       *temp_ptr_r_165_179 = 0;
    }
    return ;
  }
  temp = gsm_norm(L_ACF[0]);
  pushDbg(temp,800);
  for (i = 0; i <= 8; i++) {
    word *temp_ptr_ACF_169_185;
    temp_ptr_ACF_169_185 = &ACF[i];
    pushDbg( *temp_ptr_ACF_169_185,804);
     *temp_ptr_ACF_169_185 = (L_ACF[i] << temp >> 16);
  }
/*   Initialize array P[..] and K[..] for the recursion.
   */
  for (i = 1; i <= 7; i++) {
    word *temp_ptr_K_171_191;
    temp_ptr_K_171_191 = &K[i];
    pushDbg( *temp_ptr_K_171_191,811);
     *temp_ptr_K_171_191 = ACF[i];
  }
  for (i = 0; i <= 8; i++) {
    word *temp_ptr_P_170_193;
    temp_ptr_P_170_193 = &P[i];
    pushDbg( *temp_ptr_P_170_193,816);
     *temp_ptr_P_170_193 = ACF[i];
  }
/*   Compute reflection coefficients
   */
  for (n = 1; n <= 8; (n++ , r++)) {
    temp = P[1];
    pushDbg(temp,822);
    temp = gsm_abs(temp);
    pushDbg(temp,824);
    if (P[0] < temp) {
      for (i = n, pushDbg(i,827); i <= 8; i++) {
        word *temp_ptr_r_165_205;
        temp_ptr_r_165_205 = r++;
        pushDbg( *temp_ptr_r_165_205,829);
         *temp_ptr_r_165_205 = 0;
      }
      return ;
    }
    word *temp_ptr_r_165_209;
    temp_ptr_r_165_209 = r;
    pushDbg( *temp_ptr_r_165_209,830);
     *temp_ptr_r_165_209 = gsm_div(temp,P[0]);
    if (P[1] > 0) {
/* r[n] = sub(0, r[n]) */
      word *temp_ptr_r_165_212;
      temp_ptr_r_165_212 = r;
      pushDbg( *temp_ptr_r_165_212,834);
       *temp_ptr_r_165_212 = (-( *r));
    }
    if (n == 8) 
      return ;
/*  Schur recursion
       */
    temp = gsm_mult_r(P[1], *r);
    pushDbg(temp,836);
    word *temp_ptr_P_170_219;
    temp_ptr_P_170_219 = &P[0];
    pushDbg( *temp_ptr_P_170_219,838);
     *temp_ptr_P_170_219 = gsm_add(P[0],temp);
    for (m = 1; m <= 8 - n; m++) {
      temp = gsm_mult_r(K[m], *r);
      pushDbg(temp,844);
      word *temp_ptr_P_170_224;
      temp_ptr_P_170_224 = &P[m];
      pushDbg( *temp_ptr_P_170_224,846);
       *temp_ptr_P_170_224 = gsm_add(P[m + 1],temp);
      temp = gsm_mult_r(P[m + 1], *r);
      pushDbg(temp,850);
      word *temp_ptr_K_171_227;
      temp_ptr_K_171_227 = &K[m];
      pushDbg( *temp_ptr_K_171_227,853);
       *temp_ptr_K_171_227 = gsm_add(K[m],temp);
    }
  }
  pushDbg(n,2923);
}
/* 4.2.6 */

void Transformation_to_Log_Area_Ratios(
/* 0..7    IN/OUT */
word *r)
/*
 *  The following scaling for r[..] and LAR[..] has been used:
 *
 *  r[..]   = integer( real_r[..]*32768. ); -1 <= real_r < 1.
 *  LAR[..] = integer( real_LAR[..] * 16384 );
 *  with -1.625 <= real_LAR <= 1.625
 */
{
  register word temp;
  register int i;
/* Computation of the LAR[0..7] from the r[0..7]
   */
  for (i = 1; i <= 8; (i++ , r++)) {
    temp =  *r;
    pushDbg(temp,859);
    temp = gsm_abs(temp);
    pushDbg(temp,860);
    if (temp < 22118) {
      temp >>= 1;
    }
     else if (temp < 31130) {
      temp -= 11059;
    }
     else {
      temp -= 26112;
      temp <<= 2;
    }
    word *temp_ptr_r_235_270;
    temp_ptr_r_235_270 = r;
    pushDbg( *temp_ptr_r_235_270,867);
     *temp_ptr_r_235_270 = ((( *r) < 0?-temp : temp));
  }
  pushDbg(i,2942);
}
/* 4.2.7 */

void Quantization_and_coding(
/* [0..7]       IN/OUT  */
word *LAR)
{
  register word temp;
/*  This procedure needs four tables; the following equations
   *  give the optimum scaling for the constants:
   *  
   *  A[0..7] = integer( real_A[0..7] * 1024 )
   *  B[0..7] = integer( real_B[0..7] *  512 )
   *  MAC[0..7] = maximum of the LARc[0..7]
   *  MIC[0..7] = minimum of the LARc[0..7]
   */
#	undef STEP
#	define	STEP( A, B, MAC, MIC )		\
		temp = GSM_MULT( A,   *LAR );	\
		temp = GSM_ADD(  temp,   B );	\
		temp = GSM_ADD(  temp, 256 );	\
		temp = SASR(     temp,   9 );	\
		*LAR  =  temp>MAC ? MAC - MIC : (temp<MIC ? 0 : temp - MIC); \
		LAR++;
  temp = gsm_mult(20480, *LAR);
  pushDbg(temp,869);
  temp = gsm_add(temp,0);
  pushDbg(temp,870);
  temp = gsm_add(temp,256);
  pushDbg(temp,871);
  temp = (temp >> 9);
  pushDbg(temp,872);
  word *temp_ptr_LAR_277_300;
  temp_ptr_LAR_277_300 = LAR;
  pushDbg( *temp_ptr_LAR_277_300,874);
   *temp_ptr_LAR_277_300 = ((temp > 31?31 - - 32 : ((temp < - 32?0 : temp - - 32))));
  LAR++;
  ;
  temp = gsm_mult(20480, *LAR);
  pushDbg(temp,879);
  temp = gsm_add(temp,0);
  pushDbg(temp,880);
  temp = gsm_add(temp,256);
  pushDbg(temp,881);
  temp = (temp >> 9);
  pushDbg(temp,882);
  word *temp_ptr_LAR_277_301;
  temp_ptr_LAR_277_301 = LAR;
  pushDbg( *temp_ptr_LAR_277_301,884);
   *temp_ptr_LAR_277_301 = ((temp > 31?31 - - 32 : ((temp < - 32?0 : temp - - 32))));
  LAR++;
  ;
  temp = gsm_mult(20480, *LAR);
  pushDbg(temp,889);
  temp = gsm_add(temp,2048);
  pushDbg(temp,890);
  temp = gsm_add(temp,256);
  pushDbg(temp,891);
  temp = (temp >> 9);
  pushDbg(temp,892);
  word *temp_ptr_LAR_277_302;
  temp_ptr_LAR_277_302 = LAR;
  pushDbg( *temp_ptr_LAR_277_302,894);
   *temp_ptr_LAR_277_302 = ((temp > 15?15 - - 16 : ((temp < - 16?0 : temp - - 16))));
  LAR++;
  ;
  temp = gsm_mult(20480, *LAR);
  pushDbg(temp,899);
  temp = gsm_add(temp,(- 2560));
  pushDbg(temp,900);
  temp = gsm_add(temp,256);
  pushDbg(temp,901);
  temp = (temp >> 9);
  pushDbg(temp,902);
  word *temp_ptr_LAR_277_303;
  temp_ptr_LAR_277_303 = LAR;
  pushDbg( *temp_ptr_LAR_277_303,904);
   *temp_ptr_LAR_277_303 = ((temp > 15?15 - - 16 : ((temp < - 16?0 : temp - - 16))));
  LAR++;
  ;
  temp = gsm_mult(13964, *LAR);
  pushDbg(temp,909);
  temp = gsm_add(temp,94);
  pushDbg(temp,910);
  temp = gsm_add(temp,256);
  pushDbg(temp,911);
  temp = (temp >> 9);
  pushDbg(temp,912);
  word *temp_ptr_LAR_277_305;
  temp_ptr_LAR_277_305 = LAR;
  pushDbg( *temp_ptr_LAR_277_305,914);
   *temp_ptr_LAR_277_305 = ((temp > 7?7 - - 8 : ((temp < - 8?0 : temp - - 8))));
  LAR++;
  ;
  temp = gsm_mult(15360, *LAR);
  pushDbg(temp,919);
  temp = gsm_add(temp,(- 1792));
  pushDbg(temp,920);
  temp = gsm_add(temp,256);
  pushDbg(temp,921);
  temp = (temp >> 9);
  pushDbg(temp,922);
  word *temp_ptr_LAR_277_306;
  temp_ptr_LAR_277_306 = LAR;
  pushDbg( *temp_ptr_LAR_277_306,924);
   *temp_ptr_LAR_277_306 = ((temp > 7?7 - - 8 : ((temp < - 8?0 : temp - - 8))));
  LAR++;
  ;
  temp = gsm_mult(8534, *LAR);
  pushDbg(temp,929);
  temp = gsm_add(temp,(- 341));
  pushDbg(temp,930);
  temp = gsm_add(temp,256);
  pushDbg(temp,931);
  temp = (temp >> 9);
  pushDbg(temp,932);
  word *temp_ptr_LAR_277_307;
  temp_ptr_LAR_277_307 = LAR;
  pushDbg( *temp_ptr_LAR_277_307,934);
   *temp_ptr_LAR_277_307 = ((temp > 3?3 - - 4 : ((temp < - 4?0 : temp - - 4))));
  LAR++;
  ;
  temp = gsm_mult(9036, *LAR);
  pushDbg(temp,939);
  temp = gsm_add(temp,(- 1144));
  pushDbg(temp,940);
  temp = gsm_add(temp,256);
  pushDbg(temp,941);
  temp = (temp >> 9);
  pushDbg(temp,942);
  word *temp_ptr_LAR_277_308;
  temp_ptr_LAR_277_308 = LAR;
  pushDbg( *temp_ptr_LAR_277_308,944);
   *temp_ptr_LAR_277_308 = ((temp > 3?3 - - 4 : ((temp < - 4?0 : temp - - 4))));
  LAR++;
  ;
#	undef	STEP
}

void Gsm_LPC_Analysis(
/* 0..159 signals       IN/OUT  */
word *s,
/* 0..7   LARc's        OUT     */
word *LARc)
{
  longword L_ACF[9];
  Autocorrelation(s,L_ACF);
  Reflection_coefficients(L_ACF,LARc);
  Transformation_to_Log_Area_Ratios(LARc);
  Quantization_and_coding(LARc);
}
