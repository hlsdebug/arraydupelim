/*
 * addShBuf.h
 *
 *  Created on: Feb 13, 2016
 *      Author: legup
 */

#ifndef ADDSHBUF_H_
#define ADDSHBUF_H_

bool ispowerof2(unsigned int x) {
   return x && !(x & (x - 1));
 }


char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

//Get command option value and remove it from list.
//Useful when instantiating other frontends.
//Arguments; argv, &argc, "-option"
char* getCmdOptionAndRemove(char ** argv, int *size, const std::string & option)
{
	int commandSize = 2; // Works for <-option value> which adds to 2 items

	char ** end = argv+*size;
	char* cmdVal = 0;
    char ** itr = std::find(argv, end, option);
    int itrIndex = itr-argv;

    if (itr != end && ++itr != end)
    {
        cmdVal = *itr;

		for ( int c = itrIndex; c < *size - 1 ; c++ ){
			argv[c] = argv[c+commandSize];
		}
		*size-=commandSize;
    }
    return cmdVal;
}

//Return index of Option or NULL if not found
char** cmdOptionExists(char** begin, char** end, const std::string& option) {
	char ** index = std::find(begin, end, option);
    if(index!=end)
    	return index;
    else
    	return 0;
}


std::pair<SgVariableSymbol*,SgExpression*> getTargetSymbolAndIndex(SgExpression * assignTgtExp) {

		if (SgPointerDerefExp * pntrDerefExp = isSgPointerDerefExp(assignTgtExp)){
			std::cout<<"POINTER DEREFERENCE"<<std::endl;

			//Recognize types of pointer dereference
			SgExpression * pntrExp = pntrDerefExp->get_operand();
			if (SgUnaryOp* pntrUnOp = isSgUnaryOp(pntrExp)){ //Pointer unary arithmetic on dereference
				if(pntrUnOp->get_mode()){ //Mode is true for postfix (only applies to PlusPlus and MinusMinus Operations)
					std::cout<<"IS POSTFIX"<<std::endl;
				}else{
					std::cout<<"IS PREFIX"<<std::endl;
				}
				pntrExp = pntrUnOp->get_operand_i();
			}else if (SgBinaryOp* pntrBinOp = isSgBinaryOp(pntrExp)){ //Pointer binary arithmetic on dereference
				std::cout<<"BINARY OP POINTER ARITHMETIC"<<std::endl;
				pntrExp = pntrBinOp->get_lhs_operand();
			}else{ //Regular pointer dereference assignment
				std::cout<<"REGULAR POINTER"<<std::endl;
			}

			return std::make_pair(isSgVarRefExp(pntrExp)->get_symbol(),pntrExp); //TODO: Strip out the pointer and use it

		}else if( SgPntrArrRefExp * pntrArrRef = isSgPntrArrRefExp(assignTgtExp)){ //Left Hand Operand is an Array Reference
			std::cout<<"Pointer Array Reference"<<std::endl;

			//Recognize types of array reference
			SgExpression * arrRHSExp = pntrArrRef->get_rhs_operand();
			if(SgUnaryOp *arrUnOp = isSgUnaryOp(arrRHSExp)){ //Array index arithmetic
				if(arrUnOp->get_mode()){ //Mode is true for postfix (only applies to PlusPlus and MinusMinus Operations)
					std::cout<<"IS POSTFIX"<<std::endl;
				}else{
					std::cout<<"IS PREFIX"<<std::endl;
				}
			} else if (isSgBinaryOp(arrRHSExp)){ //Pointer binary arithmetic on dereference
				std::cout<<"BINARY OP ARRAY INDEX"<<std::endl;
			} else { //Regular array write
				std::cout<<"REGULAR ARRAY INDEX"<<std::endl;
			}

			do{
				assignTgtExp = pntrArrRef->get_lhs_operand();
				pntrArrRef =isSgPntrArrRefExp(assignTgtExp);
				//TODO: Safety net if not multi-dimensional array but other Op
			}while(!isSgVarRefExp(assignTgtExp));

			return std::make_pair(isSgVarRefExp(assignTgtExp)->get_symbol(),arrRHSExp);
		}
		else{
			std::cout<<"Unrecognized Expression"<<std::endl;
			return std::make_pair((SgVariableSymbol*)0,(SgExpression*)0);
		}
}

#endif /* ADDSHBUF_H_ */
