#!/bin/bash

cd adpcm
cp rose_adpcm.c /home/legup/Xilinx/workspace/CHSTONE_ARR/adpcm/adpcm.c

cd ../aes
cp rose_aes.c			/home/legup/Xilinx/workspace/CHSTONE_ARR/aes/aes.c			
cp rose_aes_dec.c	/home/legup/Xilinx/workspace/CHSTONE_ARR/aes/aes_dec.c	
cp rose_aes_enc.c	/home/legup/Xilinx/workspace/CHSTONE_ARR/aes/aes_enc.c	
cp rose_aes_func.c 	/home/legup/Xilinx/workspace/CHSTONE_ARR/aes/aes_func.c 	
cp rose_aes_key.c	/home/legup/Xilinx/workspace/CHSTONE_ARR/aes/aes_key.c	

cd ../blowfish
cp rose_bf.c   				/home/legup/Xilinx/workspace/CHSTONE_ARR/blowfish/bf.c   		
cp rose_bf_cfb64.c      	/home/legup/Xilinx/workspace/CHSTONE_ARR/blowfish/bf_cfb64.c
cp rose_bf_enc.c        	/home/legup/Xilinx/workspace/CHSTONE_ARR/blowfish/bf_enc.c    
cp rose_bf_skey.c       	/home/legup/Xilinx/workspace/CHSTONE_ARR/blowfish/bf_skey.c 


cd ../dfadd
cp rose_dfadd.c		/home/legup/Xilinx/workspace/CHSTONE_ARR/dfadd/dfadd.c		
cp rose_softfloat.c  /home/legup/Xilinx/workspace/CHSTONE_ARR/dfadd/softfloat.c

cd ../dfdiv
cp rose_dfdiv.c 		/home/legup/Xilinx/workspace/CHSTONE_ARR/dfdiv.c 	
cp rose_softfloat.c  /home/legup/Xilinx/workspace/CHSTONE_ARR/softfloat.c



cd ../dfmul
cp rose_dfmul.c		/home/legup/Xilinx/workspace/CHSTONE_ARR/dfmul/dfmul.c	
cp rose_softfloat.c  /home/legup/Xilinx/workspace/CHSTONE_ARR/dfmul/softfloat.c

cd ../dfsin
cp rose_dfsin.c		/home/legup/Xilinx/workspace/CHSTONE_ARR/dfsin/dfsin.c	
cp rose_softfloat.c 	/home/legup/Xilinx/workspace/CHSTONE_ARR/dfsin/softfloat.c

cd ../gsm
cp rose_gsm.c			/home/legup/Xilinx/workspace/CHSTONE_ARR/gsm/gsm.c
cp rose_add.c         /home/legup/Xilinx/workspace/CHSTONE_ARR/gsm/add.c   
cp rose_lpc.c          /home/legup/Xilinx/workspace/CHSTONE_ARR/gsm/lpc.c    

cd ../jpeg
cp rose_main.c			/home/legup/Xilinx/workspace/CHSTONE_ARR/jpeg/main.c
cp rose_chenidct.c       /home/legup/Xilinx/workspace/CHSTONE_ARR/jpeg/chenidct.c
cp rose_decode.c         /home/legup/Xilinx/workspace/CHSTONE_ARR/jpeg/decode.c
cp rose_huffman.c       /home/legup/Xilinx/workspace/CHSTONE_ARR/jpeg/huffman.c
cp rose_jpeg2bmp.c     /home/legup/Xilinx/workspace/CHSTONE_ARR/jpeg/jpeg2bmp.c
cp rose_jfif_read.c       /home/legup/Xilinx/workspace/CHSTONE_ARR/jpeg/jfif_read.c
cp rose_marker.c          /home/legup/Xilinx/workspace/CHSTONE_ARR/jpeg/marker.c  

cd ../mips
cp rose_mips.c  /home/legup/Xilinx/workspace/CHSTONE_ARR/mips/mips.c
                       
cd ../motion
cp rose_mpeg2.c			/home/legup/Xilinx/workspace/CHSTONE_ARR/motion/mpeg2.c	
cp rose_motion.c         /home/legup/Xilinx/workspace/CHSTONE_ARR/motion/motion.c 
cp rose_getbits.c         /home/legup/Xilinx/workspace/CHSTONE_ARR/motion/getbits.c 
cp rose_getvlc.c          /home/legup/Xilinx/workspace/CHSTONE_ARR/motion/getvlc.c  

cd ../sha
cp rose_sha_driver.c	/home/legup/Xilinx/workspace/CHSTONE_ARR/sha/sha_driver.c
cp rose_sha.c             /home/legup/Xilinx/workspace/CHSTONE_ARR/sha/sha.c           

cd ..

#cp -R ./ /home/legup/Xilinx/workspace/CHSTONE_SHBUF/
#rm /home/legup/Xilinx/workspace/CHSTONE_SHBUF/run.sh

#rename 's/rose_//g' ./*/*.c
