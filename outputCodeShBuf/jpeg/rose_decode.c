/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*************************************************************
Copyright (C) 1990, 1991, 1993 Andy C. Hung, all rights reserved.
PUBLIC DOMAIN LICENSE: Stanford University Portable Video Research
Group. If you use this software, you agree to the following: This
program package is purely experimental, and is licensed "as is".
Permission is granted to use, modify, and distribute this program
without charge for any purpose, provided this license/ disclaimer
notice appears in the copies.  No warranty or maintenance is given,
either expressed or implied.  In no event shall the author(s) be
liable to you or a third party for any special, incidental,
consequential, or other damages, arising out of the use or inability
to use the program for any purpose (or the loss of data), even if we
have been advised of such possibilities.  Any public reference or
advertisement of this source code should refer to it as the Portable
Video Research Group (PVRG) code, and not by any author(s) (or
Stanford University) name.
*************************************************************/
/*
************************************************************
decode.c (original: transform.c)
This file contains the reference DCT, the zig-zag and quantization
algorithms.
************************************************************
*/
/*
 *  Decoder
 *
 *  @(#) $Id: decode.c,v 1.2 2003/07/18 10:19:21 honda Exp $
 */
#include "decode.h"
#include "huffman.h"
#include "global.h"
#include "init.h"
void ChenIDct(int *x,int *y);
int rgb_buf[4][3][64];
/* Is zig-zag map for matrix -> scan array */
const int zigzag_index[64] = {(0), (1), (5), (6), (14), (15), (27), (28), (2), (4), (7), (13), (16), (26), (29), (42), (3), (8), (12), (17), (25), (30), (41), (43), (9), (11), (18), (24), (31), (40), (44), (53), (10), (19), (23), (32), (39), (45), (52), (54), (20), (22), (33), (38), (46), (51), (55), (60), (21), (34), (37), (47), (50), (56), (59), (61), (35), (36), (48), (49), (57), (58), (62), (63)};
/*
 * IZigzagMatrix() performs an inverse zig-zag translation on the
 * input imatrix and places the output in omatrix.
 */

void IZigzagMatrix(int *imatrix,int *omatrix)
{
  int i;
  for (i = 0; i < 64; i++) {
    int *temp_ptr_omatrix_78_84;
    temp_ptr_omatrix_78_84 = omatrix++;
    pushDbg( *temp_ptr_omatrix_78_84,283,omatrix);
     *temp_ptr_omatrix_78_84 = imatrix[zigzag_index[i]];
  }
}
/*
 * IQuantize() takes an input matrix and does an inverse quantization
 * and puts the output int qmatrix.
 */

void IQuantize(int *matrix,int *qmatrix)
{
  int *mptr;
  for (mptr = matrix; mptr < matrix + 64; mptr++) {
    int *temp_ptr_mptr_95_98;
    temp_ptr_mptr_95_98 = mptr;
    pushDbg( *temp_ptr_mptr_95_98,285,mptr);
     *temp_ptr_mptr_95_98 =  *mptr *  *qmatrix;
    qmatrix++;
  }
}
/*
 * PostshiftIDctMatrix() adds 128 (2048) to all 64 elements of an 8x8 matrix.
 * This results in strictly positive values for all pixel coefficients.
 */

void PostshiftIDctMatrix(int *matrix,int shift)
{
  int *mptr;
  for (mptr = matrix; mptr < matrix + 64; mptr++) {
     *mptr += shift;
  }
}
/*
 * BoundIDctMatrix bounds the inverse dct matrix so that no pixel has a
 * value greater than 255 (4095) or less than 0.
 */

void BoundIDctMatrix(int *matrix,int Bound)
{
  int *mptr;
  for (mptr = matrix; mptr < matrix + 64; mptr++) {
    if ( *mptr < 0) {
       *mptr = 0;
    }
     else if ( *mptr > Bound) {
      int *temp_ptr_mptr_125_132;
      temp_ptr_mptr_125_132 = mptr;
      pushDbg( *temp_ptr_mptr_125_132,288,mptr);
       *temp_ptr_mptr_125_132 = Bound;
    }
  }
}

void WriteOneBlock(int *store,unsigned char *out_buf,int width,int height,int voffs,int hoffs)
{
  int i;
  int e;
/* Find vertical buffer offs. */
  for (i = voffs, pushDbg(i,289,0); i < voffs + 8; i++) {
    if (i < height) {
      int diff;
      diff = width * i;
      pushDbg(diff,290,0);
      for (e = hoffs, pushDbg(e,291,0); e < hoffs + 8; e++) {
        if (e < width) {
          unsigned char *temp_ptr_out_buf_140_153;
          temp_ptr_out_buf_140_153 = &out_buf[diff + e];
          pushDbg( *temp_ptr_out_buf_140_153,292,diff + e);
           *temp_ptr_out_buf_140_153 = ((unsigned char )( *(store++)));
        }
         else {
          break; 
        }
      }
    }
     else {
      break; 
    }
  }
}
/*
 * WriteBlock() writes an array of data in the integer array pointed to
 * by store out to the driver specified by the IOB.  The integer array is
 * stored in row-major form, that is, the first row of (8) elements, the
 * second row of (8) elements....
 * ONLY for MCU 1:1:1
 */

void WriteBlock(int *store,int *p_out_vpos,int *p_out_hpos,unsigned char *p_out_buf)
{
  int voffs;
  int hoffs;
/*
     * Get vertical offsets
     */
  voffs =  *p_out_vpos * 8;
  pushDbg(voffs,293,0);
  hoffs =  *p_out_hpos * 8;
  pushDbg(hoffs,294,0);
/*
     * Write block
     */
  WriteOneBlock(store,p_out_buf,p_jinfo_image_width,p_jinfo_image_height,voffs,hoffs);
/*
     *  Add positions
     */
  p_out_hpos++;
  p_out_vpos++;
  if ( *p_out_hpos < p_jinfo_MCUWidth) {
    p_out_vpos--;
  }
   else {
/* If at end of image (width) */
     *p_out_hpos = 0;
  }
}
/*
 *  4:1:1
 */

void Write4Blocks(int *store1,int *store2,int *store3,int *store4,int *p_out_vpos,int *p_out_hpos,unsigned char *p_out_buf)
{
  int voffs;
  int hoffs;
/*
     * OX
     * XX
     */
  voffs =  *p_out_vpos * 8;
  pushDbg(voffs,295,0);
  hoffs =  *p_out_hpos * 8;
  pushDbg(hoffs,296,0);
  WriteOneBlock(store1,p_out_buf,p_jinfo_image_width,p_jinfo_image_height,voffs,hoffs);
/*
     * XO
     * XX
     */
  hoffs += 8;
  WriteOneBlock(store2,p_out_buf,p_jinfo_image_width,p_jinfo_image_height,voffs,hoffs);
/*
     * XX
     * OX
     */
  voffs += 8;
  hoffs -= 8;
  WriteOneBlock(store3,p_out_buf,p_jinfo_image_width,p_jinfo_image_height,voffs,hoffs);
/*
     * XX
     * XO
     */
  hoffs += 8;
  WriteOneBlock(store4,p_out_buf,p_jinfo_image_width,p_jinfo_image_height,voffs,hoffs);
/*
     * Add positions
     */
  int *temp_ptr_p_out_hpos_211_257;
  temp_ptr_p_out_hpos_211_257 = p_out_hpos;
  pushDbg( *temp_ptr_p_out_hpos_211_257,297,p_out_hpos);
   *temp_ptr_p_out_hpos_211_257 =  *p_out_hpos + 2;
  int *temp_ptr_p_out_vpos_211_258;
  temp_ptr_p_out_vpos_211_258 = p_out_vpos;
  pushDbg( *temp_ptr_p_out_vpos_211_258,298,p_out_vpos);
   *temp_ptr_p_out_vpos_211_258 =  *p_out_vpos + 2;
  if ( *p_out_hpos < p_jinfo_MCUWidth) {
    int *temp_ptr_p_out_vpos_211_262;
    temp_ptr_p_out_vpos_211_262 = p_out_vpos;
    pushDbg( *temp_ptr_p_out_vpos_211_262,299,p_out_vpos);
     *temp_ptr_p_out_vpos_211_262 =  *p_out_vpos - 2;
  }
   else {
/* If at end of image (width) */
     *p_out_hpos = 0;
  }
}
/*
 * Transform from Yuv into RGB
 */

void YuvToRgb(int p,int *y_buf,int *u_buf,int *v_buf)
{
  int r;
  int g;
  int b;
  int y;
  int u;
  int v;
  int i;
  for (i = 0; i < 64; i++) {
    y = y_buf[i];
    pushDbg(y,300,0);
    u = u_buf[i] - 128;
    pushDbg(u,301,0);
    v = v_buf[i] - 128;
    pushDbg(v,302,0);
    r = y * 256 + v * 359 + 128 >> 8;
    pushDbg(r,303,0);
    g = y * 256 - u * 88 - v * 182 + 128 >> 8;
    pushDbg(g,304,0);
    b = y * 256 + u * 454 + 128 >> 8;
    pushDbg(b,305,0);
    if (r < 0) 
      r = 0;
     else if (r > 255) 
      r = 255;
    if (g < 0) 
      g = 0;
     else if (g > 255) 
      g = 255;
    if (b < 0) 
      b = 0;
     else if (b > 255) 
      b = 255;
    int *temp_ptr_rgb_buf_59_303;
    temp_ptr_rgb_buf_59_303 = &rgb_buf[p][0][i];
    pushDbg( *temp_ptr_rgb_buf_59_303,306,i);
     *temp_ptr_rgb_buf_59_303 = r;
    int *temp_ptr_rgb_buf_59_304;
    temp_ptr_rgb_buf_59_304 = &rgb_buf[p][1][i];
    pushDbg( *temp_ptr_rgb_buf_59_304,307,i);
     *temp_ptr_rgb_buf_59_304 = g;
    int *temp_ptr_rgb_buf_59_305;
    temp_ptr_rgb_buf_59_305 = &rgb_buf[p][2][i];
    pushDbg( *temp_ptr_rgb_buf_59_305,308,i);
     *temp_ptr_rgb_buf_59_305 = b;
  }
}
void DecodeHuffMCU(int *out_buf,int num_cmp);
/*
 * Decode one block
 */

void decode_block(int comp_no,int *out_buf,int *HuffBuff)
{
  int QuantBuff[64];
  unsigned int *p_quant_tbl;
  DecodeHuffMCU(HuffBuff,comp_no);
  IZigzagMatrix(HuffBuff,QuantBuff);
  p_quant_tbl = &p_jinfo_quant_tbl_quantval[(int )p_jinfo_comps_info_quant_tbl_no[comp_no]][64];
  IQuantize(QuantBuff,((int *)p_quant_tbl));
  ChenIDct(QuantBuff,out_buf);
  PostshiftIDctMatrix(out_buf,128);
  BoundIDctMatrix(out_buf,255);
}

void decode_start(int *out_data_image_width,int *out_data_image_height,int *out_data_comp_vpos,int *out_data_comp_hpos)
{
  int i;
  int CurrentMCU = 0;
  int HuffBuff[3][64];
  int IDCTBuff[6][64];
/* Read buffer */
  CurHuffReadBuf = p_jinfo_jpeg_data;
/*
     * Initial value of DC element is 0
     */
  for (i = 0; i < 3; i++) {
    HuffBuff[i][0] = 0;
  }
/*
     * Set the size of image to output buffer
     */
  int *temp_ptr_out_data_image_width_340_360;
  temp_ptr_out_data_image_width_340_360 = out_data_image_width;
  pushDbg( *temp_ptr_out_data_image_width_340_360,311,out_data_image_width);
   *temp_ptr_out_data_image_width_340_360 = p_jinfo_image_width;
  int *temp_ptr_out_data_image_height_340_361;
  temp_ptr_out_data_image_height_340_361 = out_data_image_height;
  pushDbg( *temp_ptr_out_data_image_height_340_361,312,out_data_image_height);
   *temp_ptr_out_data_image_height_340_361 = p_jinfo_image_height;
/*
     * Initialize output buffer
     */
  for (i = 0; i < 3; i++) {
    out_data_comp_vpos[i] = 0;
    out_data_comp_hpos[i] = 0;
  }
  if (p_jinfo_smp_fact == 0) {
    printf("Decode 1:1:1 NumMCU = %d\n",p_jinfo_NumMCU);
/*
         * 1_1_1
         */
    while(CurrentMCU < p_jinfo_NumMCU){
      for (i = 0; i < 3; i++) {
        decode_block(i,IDCTBuff[i],HuffBuff[i]);
      }
      YuvToRgb(0,IDCTBuff[0],IDCTBuff[1],IDCTBuff[2]);
/*
             * Write
             */
      for (i = 0; i < 3; i++) {
        WriteBlock(&rgb_buf[0][i][0],&out_data_comp_vpos[i],&out_data_comp_hpos[i],&OutData_comp_buf[i][0]);
      }
      CurrentMCU++;
      pushDbg(CurrentMCU,553,0);
    }
  }
   else {
    printf("Decode 4:1:1 NumMCU = %d\n",p_jinfo_NumMCU);
/*
         * 4_1_1
         */
    while(CurrentMCU < p_jinfo_NumMCU){
/*
             * Decode Y element
	         * Decoding Y, U and V elements should be sequentially conducted for the use of Huffman table
             */
      for (i = 0; i < 4; i++) {
        decode_block(0,IDCTBuff[i],HuffBuff[0]);
      }
/* Decode U */
      decode_block(1,IDCTBuff[4],HuffBuff[1]);
/* Decode V */
      decode_block(2,IDCTBuff[5],HuffBuff[2]);
/* Transform from Yuv into RGB */
      for (i = 0; i < 4; i++) {
        YuvToRgb(i,IDCTBuff[i],IDCTBuff[4],IDCTBuff[5]);
      }
      for (i = 0; i < 3; i++) {
        Write4Blocks(&rgb_buf[0][i][0],&rgb_buf[1][i][0],&rgb_buf[2][i][0],&rgb_buf[3][i][0],&out_data_comp_vpos[i],&out_data_comp_hpos[i],&OutData_comp_buf[i][0]);
      }
      CurrentMCU += 4;
    }
  }
}
