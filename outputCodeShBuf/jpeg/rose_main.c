volatile unsigned long TRACEBUFFER[256];
unsigned char bufIndex;
unsigned long traceOut;

void pushDbg(int data,int ID,int arrIndex)
{
  TRACEBUFFER[bufIndex++] = ((unsigned long )arrIndex) << 48 | (((unsigned long )ID) << 32 | ((unsigned int )data));
}

void pushDbgLong(long dataL,int ID,int arrIndex)
{
  TRACEBUFFER[bufIndex++] = ((unsigned long )arrIndex) << 48 | ((unsigned long )ID) << 32 | ((unsigned int )(dataL >> 32));
  TRACEBUFFER[bufIndex++] = ((unsigned long )arrIndex) << 48 | ((unsigned long )ID) << 32 | ((unsigned int )dataL);
}
void traceUnload();
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>
#include "global.h"
#include "decode.h"
#include "init.h"
#include "marker.c"
#include "chenidct.c"
#include "huffman.h"
#include "decode.c"
#include "huffman.c"
#include "jfif_read.c"
#include "jpeg2bmp.c"

int jpeg_main()
{
  
#pragma HLS interface port=traceOut
  main_result = 0;
  jpeg2bmp_main();
  printf("Result: %d\n",main_result);
  if (main_result == 21745) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  traceUnload();
  return main_result;
}

void traceUnload()
{
  int i;
  for (i = 0; i < 256; i++) 
    traceOut = TRACEBUFFER[i];
  for (i = 0; i < 2; i++) 
    traceOut = __val[i];
  for (i = 0; i < 4; i++) 
    traceOut = __wchb[i];
  for (i = 0; i < 1; i++) 
    traceOut = _shortbuf[i];
  for (i = 0; i < 56; i++) 
    traceOut = __size[i];
  for (i = 0; i < 40; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 48; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 56; i++) 
    traceOut = __size[i];
  for (i = 0; i < 8; i++) 
    traceOut = __size[i];
  for (i = 0; i < 32; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = __x[i];
  for (i = 0; i < 3; i++) 
    traceOut = __old_x[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_index[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_id[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_h_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_v_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_quant_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_dc_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_ac_tbl_no[i];
  for (i = 0; i < 4; i++) 
    traceOut = p_jinfo_quant_tbl_quantval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_valptr[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_vpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_hpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_buf[i];
  for (i = 0; i < 4; i++) 
    traceOut = rgb_buf[i];
  for (i = 0; i < 64; i++) 
    traceOut = QuantBuff[i];
  for (i = 0; i < 3; i++) 
    traceOut = HuffBuff[i];
  for (i = 0; i < 6; i++) 
    traceOut = IDCTBuff[i];
  for (i = 0; i < 257; i++) 
    traceOut = huffsize[i];
  for (i = 0; i < 257; i++) 
    traceOut = huffcode[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_index[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_id[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_h_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_v_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_quant_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_dc_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_ac_tbl_no[i];
  for (i = 0; i < 4; i++) 
    traceOut = p_jinfo_quant_tbl_quantval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = __val[i];
  for (i = 0; i < 56; i++) 
    traceOut = __size[i];
  for (i = 0; i < 40; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 48; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 56; i++) 
    traceOut = __size[i];
  for (i = 0; i < 8; i++) 
    traceOut = __size[i];
  for (i = 0; i < 32; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = __x[i];
  for (i = 0; i < 3; i++) 
    traceOut = __old_x[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_vpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_hpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_buf[i];
  for (i = 0; i < 4; i++) 
    traceOut = rgb_buf[i];
  for (i = 0; i < 64; i++) 
    traceOut = QuantBuff[i];
  for (i = 0; i < 3; i++) 
    traceOut = HuffBuff[i];
  for (i = 0; i < 6; i++) 
    traceOut = IDCTBuff[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_index[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_id[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_h_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_v_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_quant_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_dc_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_ac_tbl_no[i];
  for (i = 0; i < 4; i++) 
    traceOut = p_jinfo_quant_tbl_quantval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = __val[i];
  for (i = 0; i < 40; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 48; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 56; i++) 
    traceOut = __size[i];
  for (i = 0; i < 8; i++) 
    traceOut = __size[i];
  for (i = 0; i < 32; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_vpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_hpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_buf[i];
  for (i = 0; i < 257; i++) 
    traceOut = huffsize[i];
  for (i = 0; i < 257; i++) 
    traceOut = huffcode[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_index[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_id[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_h_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_v_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_quant_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_dc_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_ac_tbl_no[i];
  for (i = 0; i < 4; i++) 
    traceOut = p_jinfo_quant_tbl_quantval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = __val[i];
  for (i = 0; i < 40; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 48; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 56; i++) 
    traceOut = __size[i];
  for (i = 0; i < 8; i++) 
    traceOut = __size[i];
  for (i = 0; i < 32; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_vpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_hpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_buf[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_index[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_id[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_h_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_v_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_quant_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_dc_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_ac_tbl_no[i];
  for (i = 0; i < 4; i++) 
    traceOut = p_jinfo_quant_tbl_quantval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = __val[i];
  for (i = 0; i < 40; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 48; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 56; i++) 
    traceOut = __size[i];
  for (i = 0; i < 8; i++) 
    traceOut = __size[i];
  for (i = 0; i < 32; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_vpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_hpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_buf[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_index[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_id[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_h_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_v_samp_factor[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_quant_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_dc_tbl_no[i];
  for (i = 0; i < 3; i++) 
    traceOut = p_jinfo_comps_info_ac_tbl_no[i];
  for (i = 0; i < 4; i++) 
    traceOut = p_jinfo_quant_tbl_quantval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_bits[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_xhuff_tbl_huffval[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_dc_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_ml[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_maxcode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_mincode[i];
  for (i = 0; i < 2; i++) 
    traceOut = p_jinfo_ac_dhuff_tbl_valptr[i];
  for (i = 0; i < 2; i++) 
    traceOut = __val[i];
  for (i = 0; i < 40; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 48; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 56; i++) 
    traceOut = __size[i];
  for (i = 0; i < 8; i++) 
    traceOut = __size[i];
  for (i = 0; i < 32; i++) 
    traceOut = __size[i];
  for (i = 0; i < 4; i++) 
    traceOut = __size[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __xsubi[i];
  for (i = 0; i < 3; i++) 
    traceOut = __seed16v[i];
  for (i = 0; i < 7; i++) 
    traceOut = __param[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_vpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_hpos[i];
  for (i = 0; i < 3; i++) 
    traceOut = OutData_comp_buf[i];
}
