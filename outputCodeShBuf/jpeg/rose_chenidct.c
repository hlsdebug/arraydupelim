/*
+--------------------------------------------------------------------------+
| CHStone : A suite of Benchmark Programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remarks :                                                              |
|    1. This source code is reformatted to follow CHStone's style.         |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       successfully executed.                                             |
|    4. Follow the copyright of each benchmark program.                    |
+--------------------------------------------------------------------------+
*/
/*
 *  IDCT transformation of Chen algorithm
 *
 *  @(#) $Id: chenidct.c,v 1.2 2003/07/18 10:19:21 honda Exp $
 */
/*************************************************************
Copyright (C) 1990, 1991, 1993 Andy C. Hung, all rights reserved.
PUBLIC DOMAIN LICENSE: Stanford University Portable Video Research
Group. If you use this software, you agree to the following: This
program package is purely experimental, and is licensed "as is".
Permission is granted to use, modify, and distribute this program
without charge for any purpose, provided this license/ disclaimer
notice appears in the copies.  No warranty or maintenance is given,
either expressed or implied.  In no event shall the author(s) be
liable to you or a third party for any special, incidental,
consequential, or other damages, arising out of the use or inability
to use the program for any purpose (or the loss of data), even if we
have been advised of such possibilities.  Any public reference or
advertisement of this source code should refer to it as the Portable
Video Research Group (PVRG) code, and not by any author(s) (or
Stanford University) name.
*************************************************************/
/*
************************************************************
chendct.c
A simple DCT algorithm that seems to have fairly nice arithmetic
properties.
W. H. Chen, C. H. Smith and S. C. Fralick "A fast computational
algorithm for the discrete cosine transform," IEEE Trans. Commun.,
vol. COM-25, pp. 1004-1009, Sept 1977.
************************************************************
*/
#define LS(r,s) ((r) << (s))
#define RS(r,s) ((r) >> (s))       /* Caution with rounding... */
#define MSCALE(expr)  RS((expr),9)
/* Cos constants */
#define c1d4 362L
#define c1d8 473L
#define c3d8 196L
#define c1d16 502L
#define c3d16 426L
#define c5d16 284L
#define c7d16 100L
/*
 *
 * ChenIDCT() implements the Chen inverse dct. Note that there are two
 * input vectors that represent x=input, and y=output, and must be
 * defined (and storage allocated) before this routine is called.
 */

void ChenIDct(int *x,int *y)
{
  register int i;
  register int *aptr;
  register int a0;
  register int a1;
  register int a2;
  register int a3;
  register int b0;
  register int b1;
  register int b2;
  register int b3;
  register int c0;
  register int c1;
  register int c2;
  register int c3;
/* Loop over columns */
  for (i = 0; i < 8; i++) {
    aptr = x + i;
    b0 =  *aptr << 2;
    pushDbg(b0,206,0);
    aptr += 8;
    a0 =  *aptr << 2;
    pushDbg(a0,207,0);
    aptr += 8;
    b2 =  *aptr << 2;
    pushDbg(b2,208,0);
    aptr += 8;
    a1 =  *aptr << 2;
    pushDbg(a1,209,0);
    aptr += 8;
    b1 =  *aptr << 2;
    pushDbg(b1,210,0);
    aptr += 8;
    a2 =  *aptr << 2;
    pushDbg(a2,211,0);
    aptr += 8;
    b3 =  *aptr << 2;
    pushDbg(b3,212,0);
    aptr += 8;
    a3 =  *aptr << 2;
    pushDbg(a3,213,0);
/* Split into even mode  b0 = x0  b1 = x4  b2 = x2  b3 = x6.
	 And the odd terms a0 = x1 a1 = x3 a2 = x5 a3 = x7.
	 */
    c0 = (100L * a0 - 502L * a3 >> 9);
    pushDbg(c0,214,0);
    c1 = (426L * a2 - 284L * a1 >> 9);
    pushDbg(c1,215,0);
    c2 = (426L * a1 + 284L * a2 >> 9);
    pushDbg(c2,216,0);
    c3 = (502L * a0 + 100L * a3 >> 9);
    pushDbg(c3,217,0);
/* First Butterfly on even terms.*/
    a0 = (362L * (b0 + b1) >> 9);
    pushDbg(a0,218,0);
    a1 = (362L * (b0 - b1) >> 9);
    pushDbg(a1,219,0);
    a2 = (196L * b2 - 473L * b3 >> 9);
    pushDbg(a2,220,0);
    a3 = (473L * b2 + 196L * b3 >> 9);
    pushDbg(a3,221,0);
    b0 = a0 + a3;
    pushDbg(b0,222,0);
    b1 = a1 + a2;
    pushDbg(b1,223,0);
    b2 = a1 - a2;
    pushDbg(b2,224,0);
    b3 = a0 - a3;
    pushDbg(b3,225,0);
/* Second Butterfly */
    a0 = c0 + c1;
    pushDbg(a0,226,0);
    a1 = c0 - c1;
    pushDbg(a1,227,0);
    a2 = c3 - c2;
    pushDbg(a2,228,0);
    a3 = c3 + c2;
    pushDbg(a3,229,0);
    c0 = a0;
    pushDbg(c0,230,0);
    c1 = (362L * (a2 - a1) >> 9);
    pushDbg(c1,231,0);
    c2 = (362L * (a2 + a1) >> 9);
    pushDbg(c2,232,0);
    c3 = a3;
    pushDbg(c3,233,0);
    aptr = y + i;
    int *temp_ptr_aptr_81_142;
    temp_ptr_aptr_81_142 = aptr;
    pushDbg( *temp_ptr_aptr_81_142,235,aptr);
     *temp_ptr_aptr_81_142 = b0 + c3;
    aptr += 8;
    int *temp_ptr_aptr_81_144;
    temp_ptr_aptr_81_144 = aptr;
    pushDbg( *temp_ptr_aptr_81_144,236,aptr);
     *temp_ptr_aptr_81_144 = b1 + c2;
    aptr += 8;
    int *temp_ptr_aptr_81_146;
    temp_ptr_aptr_81_146 = aptr;
    pushDbg( *temp_ptr_aptr_81_146,237,aptr);
     *temp_ptr_aptr_81_146 = b2 + c1;
    aptr += 8;
    int *temp_ptr_aptr_81_148;
    temp_ptr_aptr_81_148 = aptr;
    pushDbg( *temp_ptr_aptr_81_148,238,aptr);
     *temp_ptr_aptr_81_148 = b3 + c0;
    aptr += 8;
    int *temp_ptr_aptr_81_150;
    temp_ptr_aptr_81_150 = aptr;
    pushDbg( *temp_ptr_aptr_81_150,239,aptr);
     *temp_ptr_aptr_81_150 = b3 - c0;
    aptr += 8;
    int *temp_ptr_aptr_81_152;
    temp_ptr_aptr_81_152 = aptr;
    pushDbg( *temp_ptr_aptr_81_152,240,aptr);
     *temp_ptr_aptr_81_152 = b2 - c1;
    aptr += 8;
    int *temp_ptr_aptr_81_154;
    temp_ptr_aptr_81_154 = aptr;
    pushDbg( *temp_ptr_aptr_81_154,241,aptr);
     *temp_ptr_aptr_81_154 = b1 - c2;
    aptr += 8;
    int *temp_ptr_aptr_81_156;
    temp_ptr_aptr_81_156 = aptr;
    pushDbg( *temp_ptr_aptr_81_156,242,aptr);
     *temp_ptr_aptr_81_156 = b0 - c3;
  }
/* Loop over rows */
  for (i = 0; i < 8; i++) {
    aptr = y + (i << 3);
    b0 =  *(aptr++);
    pushDbg(b0,244,0);
    a0 =  *(aptr++);
    pushDbg(a0,245,0);
    b2 =  *(aptr++);
    pushDbg(b2,246,0);
    a1 =  *(aptr++);
    pushDbg(a1,247,0);
    b1 =  *(aptr++);
    pushDbg(b1,248,0);
    a2 =  *(aptr++);
    pushDbg(a2,249,0);
    b3 =  *(aptr++);
    pushDbg(b3,250,0);
    a3 =  *aptr;
    pushDbg(a3,251,0);
/*
	Split into even mode  b0 = x0  b1 = x4  b2 = x2  b3 = x6.
	And the odd terms a0 = x1 a1 = x3 a2 = x5 a3 = x7.
	*/
    c0 = (100L * a0 - 502L * a3 >> 9);
    pushDbg(c0,252,0);
    c1 = (426L * a2 - 284L * a1 >> 9);
    pushDbg(c1,253,0);
    c2 = (426L * a1 + 284L * a2 >> 9);
    pushDbg(c2,254,0);
    c3 = (502L * a0 + 100L * a3 >> 9);
    pushDbg(c3,255,0);
/* First Butterfly on even terms.*/
    a0 = (362L * (b0 + b1) >> 9);
    pushDbg(a0,256,0);
    a1 = (362L * (b0 - b1) >> 9);
    pushDbg(a1,257,0);
    a2 = (196L * b2 - 473L * b3 >> 9);
    pushDbg(a2,258,0);
    a3 = (473L * b2 + 196L * b3 >> 9);
    pushDbg(a3,259,0);
/* Calculate last set of b's */
    b0 = a0 + a3;
    pushDbg(b0,260,0);
    b1 = a1 + a2;
    pushDbg(b1,261,0);
    b2 = a1 - a2;
    pushDbg(b2,262,0);
    b3 = a0 - a3;
    pushDbg(b3,263,0);
/* Second Butterfly */
    a0 = c0 + c1;
    pushDbg(a0,264,0);
    a1 = c0 - c1;
    pushDbg(a1,265,0);
    a2 = c3 - c2;
    pushDbg(a2,266,0);
    a3 = c3 + c2;
    pushDbg(a3,267,0);
    c0 = a0;
    pushDbg(c0,268,0);
    c1 = (362L * (a2 - a1) >> 9);
    pushDbg(c1,269,0);
    c2 = (362L * (a2 + a1) >> 9);
    pushDbg(c2,270,0);
    c3 = a3;
    pushDbg(c3,271,0);
    aptr = y + (i << 3);
    int *temp_ptr_aptr_81_211;
    temp_ptr_aptr_81_211 = aptr++;
    pushDbg( *temp_ptr_aptr_81_211,273,aptr);
     *temp_ptr_aptr_81_211 = b0 + c3;
    int *temp_ptr_aptr_81_212;
    temp_ptr_aptr_81_212 = aptr++;
    pushDbg( *temp_ptr_aptr_81_212,274,aptr);
     *temp_ptr_aptr_81_212 = b1 + c2;
    int *temp_ptr_aptr_81_213;
    temp_ptr_aptr_81_213 = aptr++;
    pushDbg( *temp_ptr_aptr_81_213,275,aptr);
     *temp_ptr_aptr_81_213 = b2 + c1;
    int *temp_ptr_aptr_81_214;
    temp_ptr_aptr_81_214 = aptr++;
    pushDbg( *temp_ptr_aptr_81_214,276,aptr);
     *temp_ptr_aptr_81_214 = b3 + c0;
    int *temp_ptr_aptr_81_215;
    temp_ptr_aptr_81_215 = aptr++;
    pushDbg( *temp_ptr_aptr_81_215,277,aptr);
     *temp_ptr_aptr_81_215 = b3 - c0;
    int *temp_ptr_aptr_81_216;
    temp_ptr_aptr_81_216 = aptr++;
    pushDbg( *temp_ptr_aptr_81_216,278,aptr);
     *temp_ptr_aptr_81_216 = b2 - c1;
    int *temp_ptr_aptr_81_217;
    temp_ptr_aptr_81_217 = aptr++;
    pushDbg( *temp_ptr_aptr_81_217,279,aptr);
     *temp_ptr_aptr_81_217 = b1 - c2;
    int *temp_ptr_aptr_81_218;
    temp_ptr_aptr_81_218 = aptr;
    pushDbg( *temp_ptr_aptr_81_218,280,aptr);
     *temp_ptr_aptr_81_218 = b0 - c3;
  }
/*
    Retrieve correct accuracy. We have additional factor
    of 16 that must be removed.
   */
  for ((i = 0 , aptr = y); i < 64; (i++ , aptr++)) {
    int *temp_ptr_aptr_81_227;
    temp_ptr_aptr_81_227 = aptr;
    pushDbg( *temp_ptr_aptr_81_227,282,aptr);
     *temp_ptr_aptr_81_227 = (( *aptr < 0? *aptr - 8 :  *aptr + 8)) / 16;
  }
  pushDbg(i,534,0);
}
/*END*/
