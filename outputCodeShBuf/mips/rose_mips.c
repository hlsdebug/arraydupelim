volatile unsigned long TRACEBUFFER[256];
unsigned char bufIndex;
unsigned long traceOut;

void pushDbg(int data,int ID,int arrIndex)
{
  TRACEBUFFER[bufIndex++] = ((unsigned long )arrIndex) << 48 | (((unsigned long )ID) << 32 | ((unsigned int )data));
}

void pushDbgLong(long dataL,int ID,int arrIndex)
{
  TRACEBUFFER[bufIndex++] = ((unsigned long )arrIndex) << 48 | ((unsigned long )ID) << 32 | ((unsigned int )(dataL >> 32));
  TRACEBUFFER[bufIndex++] = ((unsigned long )arrIndex) << 48 | ((unsigned long )ID) << 32 | ((unsigned int )dataL);
}
void traceUnload();
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
* Copyright (C) 2008
* Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
* Nagoya University, Japan
* All rights reserved.
*
* Disclaimer of Warranty
*
* These software programs are available to the user without any license fee or
* royalty on an "as is" basis. The authors disclaims any and all warranties, 
* whether express, implied, or statuary, including any implied warranties or 
* merchantability or of fitness for a particular purpose. In no event shall the
* copyright-holder be liable for any incidental, punitive, or consequential damages
* of any kind whatsoever arising from the use of these programs. This disclaimer
* of warranty extends to the user of these programs and user's customers, employees,
* agents, transferees, successors, and assigns.
*
*/
#include <stdio.h>
int main_result;
#define R 0
#define ADDU 33
#define SUBU 35
#define MULT 24
#define MULTU 25
#define MFHI 16
#define MFLO 18
#define AND 36
#define OR 37
#define XOR 38
#define SLL 0
#define SRL 2
#define SLLV 4
#define SRLV 6
#define SLT 42
#define SLTU 43
#define JR 8
#define J 2
#define JAL 3
#define ADDIU 9
#define ANDI 12
#define ORI 13
#define XORI 14
#define LW 35
#define SW 43
#define LUI 15
#define BEQ 4
#define BNE 5
#define BGEZ 1
#define SLTI 10
#define SLTIU 11
#include "imem.h"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     A : input data                                                       |
|     outData : expected output data                                       |
+--------------------------------------------------------------------------+
*/
const int A[8] = {(22), (5), (- 9), (3), (- 17), (38), (0), (11)};
const int outData[8] = {(- 17), (- 9), (0), (3), (5), (11), (22), (38)};
#define IADDR(x)	(((x)&0x000000ff)>>2)
#define DADDR(x)	(((x)&0x000000ff)>>2)

int mips_main()
{
  
#pragma HLS interface port=traceOut
  long long hilo;
  int reg[32];
  int Hi = 0;
  int Lo = 0;
  int pc = 0;
  int dmem[64] = {(0)};
  int j;
  unsigned int ins;
  int op;
  int rs;
  int rt;
  int rd;
  int shamt;
  int funct;
  short address;
  int tgtadr;
  while(1){
    int i;
    int n_inst;
    n_inst = 0;
    main_result = 0;
    for (i = 0; i < 32; i++) {
      reg[i] = 0;
    }
    reg[29] = 0x7fffeffc;
    for (i = 0; i < 8; i++) {
      int *temp_ptr_dmem_105_134;
      temp_ptr_dmem_105_134 = &dmem[i];
      pushDbg( *temp_ptr_dmem_105_134,10,i);
       *temp_ptr_dmem_105_134 = A[i];
    }
    pc = 0x00400000;
    do {
      ins = imem[(pc & 0x000000ff) >> 2];
      pushDbg(ins,14,0);
      pc = pc + 4;
      pushDbg(pc,18,0);
      op = (ins >> 26);
      pushDbg(op,20,0);
      switch(op){
        case 0:
{
          funct = (ins & 0x3f);
          pushDbg(funct,22,0);
          shamt = (ins >> 6 & 0x1f);
          pushDbg(shamt,24,0);
          rd = (ins >> 11 & 0x1f);
          pushDbg(rd,27,0);
          rt = (ins >> 16 & 0x1f);
          pushDbg(rt,30,0);
          rs = (ins >> 21 & 0x1f);
          pushDbg(rs,33,0);
          switch(funct){
            case 33:
{
              int *temp_ptr_reg_101_159;
              temp_ptr_reg_101_159 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_159,36,rd);
               *temp_ptr_reg_101_159 = reg[rs] + reg[rt];
              break; 
            }
            case 35:
{
              int *temp_ptr_reg_101_162;
              temp_ptr_reg_101_162 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_162,41,rd);
               *temp_ptr_reg_101_162 = reg[rs] - reg[rt];
              break; 
            }
            case 24:
{
              hilo = ((long long )reg[rs]) * ((long long )reg[rt]);
              pushDbgLong(hilo,46,0);
              Lo = (hilo & 0x00000000ffffffffULL);
              pushDbg(Lo,50,0);
              Hi = (((int )(hilo >> 32)) & 0xffffffffUL);
              pushDbg(Hi,52,0);
              break; 
            }
            case 25:
{
              hilo = (((unsigned long long )reg[rs]) * ((unsigned long long )reg[rt]));
              pushDbgLong(hilo,55,0);
              Lo = (hilo & 0x00000000ffffffffULL);
              pushDbg(Lo,59,0);
              Hi = (((int )(hilo >> 32)) & 0xffffffffUL);
              pushDbg(Hi,61,0);
              break; 
            }
            case 16:
{
              int *temp_ptr_reg_101_179;
              temp_ptr_reg_101_179 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_179,64,rd);
               *temp_ptr_reg_101_179 = Hi;
              break; 
            }
            case 18:
{
              int *temp_ptr_reg_101_182;
              temp_ptr_reg_101_182 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_182,66,rd);
               *temp_ptr_reg_101_182 = Lo;
              break; 
            }
            case 36:
{
              int *temp_ptr_reg_101_186;
              temp_ptr_reg_101_186 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_186,68,rd);
               *temp_ptr_reg_101_186 = reg[rs] & reg[rt];
              break; 
            }
            case 37:
{
              int *temp_ptr_reg_101_189;
              temp_ptr_reg_101_189 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_189,73,rd);
               *temp_ptr_reg_101_189 = reg[rs] | reg[rt];
              break; 
            }
            case 38:
{
              int *temp_ptr_reg_101_192;
              temp_ptr_reg_101_192 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_192,78,rd);
               *temp_ptr_reg_101_192 = reg[rs] ^ reg[rt];
              break; 
            }
            case 0:
{
              int *temp_ptr_reg_101_195;
              temp_ptr_reg_101_195 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_195,83,rd);
               *temp_ptr_reg_101_195 = reg[rt] << shamt;
              break; 
            }
            case 2:
{
              int *temp_ptr_reg_101_198;
              temp_ptr_reg_101_198 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_198,87,rd);
               *temp_ptr_reg_101_198 = reg[rt] >> shamt;
              break; 
            }
            case 4:
{
              int *temp_ptr_reg_101_201;
              temp_ptr_reg_101_201 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_201,91,rd);
               *temp_ptr_reg_101_201 = reg[rt] << reg[rs];
              break; 
            }
            case 6:
{
              int *temp_ptr_reg_101_204;
              temp_ptr_reg_101_204 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_204,96,rd);
               *temp_ptr_reg_101_204 = reg[rt] >> reg[rs];
              break; 
            }
            case 42:
{
              int *temp_ptr_reg_101_208;
              temp_ptr_reg_101_208 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_208,101,rd);
               *temp_ptr_reg_101_208 = reg[rs] < reg[rt];
              break; 
            }
            case 43:
{
              int *temp_ptr_reg_101_211;
              temp_ptr_reg_101_211 = &reg[rd];
              pushDbg( *temp_ptr_reg_101_211,106,rd);
               *temp_ptr_reg_101_211 = ((unsigned int )reg[rs]) < ((unsigned int )reg[rt]);
              break; 
            }
            case 8:
{
              pc = reg[rs];
              pushDbg(pc,111,0);
              break; 
            }
            default:
{
// error
              pc = 0;
              break; 
            }
          }
          break; 
        }
        case 2:
{
          tgtadr = (ins & 0x3ffffff);
          pushDbg(tgtadr,114,0);
          pc = tgtadr << 2;
          pushDbg(pc,116,0);
          break; 
        }
        case 3:
{
          tgtadr = (ins & 0x3ffffff);
          pushDbg(tgtadr,118,0);
          int *temp_ptr_reg_101_229;
          temp_ptr_reg_101_229 = &reg[31];
          pushDbg( *temp_ptr_reg_101_229,120,31);
           *temp_ptr_reg_101_229 = pc;
          pc = tgtadr << 2;
          pushDbg(pc,122,0);
          break; 
        }
        default:
{
          address = (ins & 0xffff);
          pushDbg(address,124,0);
          rt = (ins >> 16 & 0x1f);
          pushDbg(rt,126,0);
          rs = (ins >> 21 & 0x1f);
          pushDbg(rs,129,0);
          switch(op){
            case 9:
{
              int *temp_ptr_reg_101_241;
              temp_ptr_reg_101_241 = &reg[rt];
              pushDbg( *temp_ptr_reg_101_241,132,rt);
               *temp_ptr_reg_101_241 = reg[rs] + address;
              break; 
            }
            case 12:
{
              int *temp_ptr_reg_101_245;
              temp_ptr_reg_101_245 = &reg[rt];
              pushDbg( *temp_ptr_reg_101_245,136,rt);
               *temp_ptr_reg_101_245 = reg[rs] & ((unsigned short )address);
              break; 
            }
            case 13:
{
              int *temp_ptr_reg_101_248;
              temp_ptr_reg_101_248 = &reg[rt];
              pushDbg( *temp_ptr_reg_101_248,140,rt);
               *temp_ptr_reg_101_248 = reg[rs] | ((unsigned short )address);
              break; 
            }
            case 14:
{
              int *temp_ptr_reg_101_251;
              temp_ptr_reg_101_251 = &reg[rt];
              pushDbg( *temp_ptr_reg_101_251,144,rt);
               *temp_ptr_reg_101_251 = reg[rs] ^ ((unsigned short )address);
              break; 
            }
            case 35:
{
              int *temp_ptr_reg_101_255;
              temp_ptr_reg_101_255 = &reg[rt];
              pushDbg( *temp_ptr_reg_101_255,148,rt);
               *temp_ptr_reg_101_255 = dmem[(reg[rs] + address & 0x000000ff) >> 2];
              break; 
            }
            case 43:
{
              int *temp_ptr_dmem_105_258;
              temp_ptr_dmem_105_258 = &dmem[(reg[rs] + ((int )address) & 0x000000ff) >> 2];
              pushDbg( *temp_ptr_dmem_105_258,155,(reg[rs] + ((int )address) & 0x000000ff) >> 2);
               *temp_ptr_dmem_105_258 = reg[rt];
              break; 
            }
            case 15:
{
              int *temp_ptr_reg_101_262;
              temp_ptr_reg_101_262 = &reg[rt];
              pushDbg( *temp_ptr_reg_101_262,162,rt);
               *temp_ptr_reg_101_262 = address << 16;
              break; 
            }
            case 4:
{
              if (reg[rs] == reg[rt]) {
                pc = pc - 4 + (address << 2);
                pushDbg(pc,168,0);
              }
              break; 
            }
            case 5:
{
              if (reg[rs] != reg[rt]) {
                pc = pc - 4 + (address << 2);
                pushDbg(pc,175,0);
              }
              break; 
            }
            case 1:
{
              if (reg[rs] >= 0) {
                pc = pc - 4 + (address << 2);
                pushDbg(pc,181,0);
              }
              break; 
            }
            case 10:
{
              int *temp_ptr_reg_101_279;
              temp_ptr_reg_101_279 = &reg[rt];
              pushDbg( *temp_ptr_reg_101_279,185,rt);
               *temp_ptr_reg_101_279 = reg[rs] < address;
              break; 
            }
            case 11:
{
              int *temp_ptr_reg_101_283;
              temp_ptr_reg_101_283 = &reg[rt];
              pushDbg( *temp_ptr_reg_101_283,189,rt);
               *temp_ptr_reg_101_283 = ((unsigned int )reg[rs]) < ((unsigned short )address);
              break; 
            }
            default:
{
/* error */
              pc = 0;
              break; 
            }
          }
          break; 
        }
      }
      reg[0] = 0;
      n_inst = n_inst + 1;
      pushDbg(n_inst,196,0);
    }while (pc != 0);
    main_result += n_inst == 611;
    for (j = 0; j < 8; j++) {
      main_result += dmem[j] == outData[j];
    }
    printf("Result: %d\n",main_result);
    if (main_result == 9) {
      printf("RESULT: PASS\n");
    }
     else {
      printf("RESULT: FAIL\n");
    }
    return main_result;
  }
}

void traceUnload()
{
  int i;
  for (i = 0; i < 256; i++) 
    traceOut = TRACEBUFFER[i];
  for (i = 0; i < 2; i++) 
    traceOut = __val[i];
  for (i = 0; i < 4; i++) 
    traceOut = __wchb[i];
  for (i = 0; i < 1; i++) 
    traceOut = _shortbuf[i];
  for (i = 0; i < 32; i++) 
    traceOut = reg[i];
  for (i = 0; i < 64; i++) 
    traceOut = dmem[i];
}
