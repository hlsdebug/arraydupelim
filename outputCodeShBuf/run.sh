#!/bin/bash

rm ./*/*.c

cd adpcm
../../addShBuf --top adpcm_main ../../chstone/adpcm/adpcm.c | tee ./log.log
cd ../aes
../../addShBuf --top aes_main ../../chstone/aes/aes.c ../../chstone/aes/aes_dec.c ../../chstone/aes/aes_enc.c ../../chstone/aes/aes_func.c ../../chstone/aes/aes_key.c | tee ./log.log
cd ../blowfish
../../addShBuf --top blowfish_main ../../chstone/blowfish/bf.c ../../chstone/blowfish/bf_cfb64.c ../../chstone/blowfish/bf_enc.c ../../chstone/blowfish/bf_skey.c  | tee ./log.log
cd ../dfadd
../../addShBuf --top dfadd_main ../../chstone/dfadd/dfadd.c ../../chstone/dfadd/softfloat.c  | tee ./log.log
cd ../dfdiv
../../addShBuf --top dfdiv_main ../../chstone/dfdiv/dfdiv.c ../../chstone/dfdiv/softfloat.c  | tee ./log.log
cd ../dfmul
../../addShBuf --top dfmul_main ../../chstone/dfmul/dfmul.c ../../chstone/dfmul/softfloat.c  | tee ./log.log
cd ../dfsin
../../addShBuf --top dfsin_main ../../chstone/dfsin/dfsin.c ../../chstone/dfsin/softfloat.c  | tee ./log.log
cd ../gsm
../../addShBuf --top gsm_main ../../chstone/gsm/gsm.c ../../chstone/gsm/add.c ../../chstone/gsm/lpc.c  | tee ./log.log
cd ../jpeg
../../addShBuf --top jpeg_main ../../chstone/jpeg/main.c ../../chstone/jpeg/chenidct.c ../../chstone/jpeg/decode.c ../../chstone/jpeg/huffman.c ../../chstone/jpeg/jpeg2bmp.c ../../chstone/jpeg/jfif_read.c ../../chstone/jpeg/marker.c  | tee ./log.log
cd ../mips
../../addShBuf --top mips_main ../../chstone/mips/mips.c  | tee ./log.log
cd ../motion
../../addShBuf --top motion_main ../../chstone/motion/mpeg2.c ../../chstone/motion/motion.c ../../chstone/motion/getbits.c ../../chstone/motion/getvlc.c  | tee ./log.log
cd ../sha
../../addShBuf --top sha_main ../../chstone/sha/sha_driver.c ../../chstone/sha/sha.c  | tee ./log.log

cd ..

#cp -R ./ /home/legup/Xilinx/workspace/CHSTONE_SHBUF/
#rm /home/legup/Xilinx/workspace/CHSTONE_SHBUF/run.sh

#rename 's/rose_//g' ./*/*.c
